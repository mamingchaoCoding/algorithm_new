package com.mamingchao.concurrent.fromHashtableToHCM;

/**
 * Created by mamingchao on 2020/11/3.
 */
public class Constants {

    public final static int COUNT = 1000000;
    public final static int THREAD_NUM = 100;
}
