package com.mamingchao.concurrent.cas;

/**
 * Created by mamingchao on 2020/7/6.
 */
public class SynchonicTest {
    static long count1 = 0;
    static Object lock = new Object();

    public static void main(String[] args) throws Exception{
        Thread[] threads = new Thread[1000];

        for (int i=0;i< threads.length;i++) {
            threads[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int k=0;k< 100000;k ++)
                        synchronized (lock) {
                            count1++;
                        }
                }
            });
        }

        Long start = System.currentTimeMillis();

        for (int i=0;i< threads.length;i++) {
            threads[i].start();
        }

        for (int i=0;i< threads.length;i++) {
            threads[i].join();
        }

        Long end = System.currentTimeMillis();

        System.out.println("Atomic long time cost-" + (end - start));
    }
}
