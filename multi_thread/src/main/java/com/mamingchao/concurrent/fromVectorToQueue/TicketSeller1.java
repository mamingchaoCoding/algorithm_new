package com.mamingchao.concurrent.fromVectorToQueue;

import java.util.ArrayList;
import java.util.List;

/**
 * 这个实现，会出现超卖的现象，理论上是这样
 *
 * runnable 实现里，如果线程数少（10） 睡一段时间，演示的效果能出来
 * 如果线程数多（100个）不睡 效果也能出来
 *
 * Created by mamingchao on 2020/11/4.
 */
public class TicketSeller1 {
        static List<Ticket> pool = new ArrayList<>();

    static {
        for (int i = 0; i < 100000; i++) {
            pool.add(new Ticket("票号--" + i));
        }
    }

    public static void main(String[] args) {

        for (int i = 0; i <100; i++) {
            new Thread(()->{
                while (pool.size()>0) {
//                    try {
//                        TimeUnit.MILLISECONDS.sleep(10);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                    System.out.println("窗口售票--" + pool.remove(0) );
                }
            }).start();

        }

//        for (int i = 0; i < threads.length; i++) {
//            threads[i].start();
//            try {
//                threads[i].join();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }

//        System.out.println("剩余的火车票--" + pool.size());
    }

    static class Ticket{
        String ticketNo;

        public Ticket(String ticketNo) {
            this.ticketNo = ticketNo;
        }

        @Override
        public String toString() {
            return "Ticket{" +
                    "ticketNo='" + ticketNo + '\'' +
                    '}';
        }
    }
}
