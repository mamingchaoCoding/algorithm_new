package com.mamingchao.concurrent.four_thread_creation_way;

import java.util.concurrent.locks.ReentrantLock;

/**
 *  这是通过implements Runable创建进程
 *
 * Created by mamingchao on 2020/9/22.
 *
 */
public class ImplementsRunnable implements Runnable{

    static ReentrantLock fairLock = new ReentrantLock(true);

    private String threadName;

    public ImplementsRunnable(String threadName) {
        this.threadName = threadName;
    }

    /**
     * 重写run方式
     * 这个地方 写不写 Override都可以
     */
    @Override
    public void run() {
        for (int i = 0; i <26; i++) {
            fairLock.lock();
            System.out.println(threadName + "-" +i);
            fairLock.unlock();
        }
    }

    public static void main(String[] args) {
        ImplementsRunnable t1 = new ImplementsRunnable("t1");
        ImplementsRunnable t2 = new ImplementsRunnable("t2");
        t1.run();
        t2.run();
    }

}
