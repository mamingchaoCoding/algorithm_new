package com.mamingchao.concurrent.four_thread_creation_way;

import java.util.concurrent.locks.ReentrantLock;

/**
 *  这是通过extends thread 创建进程
 *
 * Created by mamingchao on 2020/9/22.
 */
public class ExtendThread extends Thread{

    static ReentrantLock fairLock = new ReentrantLock(true);

    private String threadName;

    public ExtendThread(String threadName) {
        this.threadName = threadName;
    }


    public void run() {
        for (int i = 0; i <26; i++) {
            fairLock.lock();
            System.out.println(threadName + "-" +i);
            fairLock.unlock();
        }
    }

    public static void main(String[] args) {
        ExtendThread t1 = new ExtendThread("t1");
        ExtendThread t2 = new ExtendThread("t2");
        t1.start();
        t2.start();
    }

}
