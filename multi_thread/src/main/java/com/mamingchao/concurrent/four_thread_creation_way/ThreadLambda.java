package com.mamingchao.concurrent.four_thread_creation_way;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.locks.ReentrantLock;

/**
 *  这是通过lambda 创建进程
 *
 * Created by mamingchao on 2020/9/22.
 *
 */
public class ThreadLambda{

    static ReentrantLock fairLock = new ReentrantLock(true);

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        new Thread(() -> {
            for (int i = 1; i <27; i++) {
                fairLock.lock();
                System.out.println(i);
                fairLock.unlock();
            }

        },"t1").start();


        new Thread(() -> {
            char a = 'A';
            for (int i = 0; i <26; i++) {
                fairLock.lock();
                System.out.println(a);
                a++;
                fairLock.unlock();
            }

        },"t2").start();

    }

}
