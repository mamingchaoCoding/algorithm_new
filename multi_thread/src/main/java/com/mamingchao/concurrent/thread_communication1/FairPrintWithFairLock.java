package com.mamingchao.concurrent.thread_communication1;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 这是自己实现的
 * 结果不是所期望的，需要深入理解 ReentrantLock 公平锁的含义
 *
 * 学完AQS，再理解一下 ReentrantLock 的实现机制
 *
 * 学完读源码的方式
 *
 * Created by mamingchao on 2020/9/22.
 */
public class FairPrintWithFairLock{

    static ReentrantLock fairLock = new ReentrantLock(true);

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        new Thread(() -> {
            for (int i = 1; i <27; i++) {
                fairLock.lock();
                System.out.println(i);
                fairLock.unlock();
            }

        },"t1").start();


        new Thread(() -> {
            char a = 'A';
            for (int i = 0; i <26; i++) {
                fairLock.lock();
                System.out.println(a);
                a++;
                fairLock.unlock();
            }

        },"t2").start();

    }
}
