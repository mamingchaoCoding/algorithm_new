package com.mamingchao.concurrent.thread_communication1;

import java.util.concurrent.locks.LockSupport;

/**
 * 成了！ 牛逼！
 * 哈哈哈哈！
 *
 * Created by mamingchao on 2020/9/22.
 */
public class FairPrintWithLockSupport {

    static Thread t1 = null,t2 = null;

    public static void main(String[] args) {

        t1 = new Thread(() -> {
            for (int i = 1; i <27; i++) {
                LockSupport.park();
                System.out.println(i);
                LockSupport.unpark(t2);
            }

        },"t1");


        t2 = new Thread(() -> {
            char a = 'A';
            for (int i = 0; i <26; i++) {
                if (i >0) {
                    LockSupport.park();
                }
                System.out.println(a);
                a++;
                LockSupport.unpark(t1);

            }

        },"t2");

        t2.start();
        t1.start();

    }
}
