package com.mamingchao.concurrent.threads_communication3;

import java.util.ArrayList;
import java.util.List;

/**
 * 让b在a里面start，也就是a控制了什么时候b开始执行
 * b start后，让b join过来
 *
 * 这样就不用什么锁了
 *
 * Created by mamingchao on 2020/9/19.
 */
public class WithThreadJoin {


    static List<Integer> lists = new ArrayList<>();

    public static void main(String[] args) {

        Thread b = new Thread( ()->{
            System.out.println("Thread b has receive the notification");

        });


        Thread a = new Thread(() -> {

            for (int i = 0; i < 10; i++) {
                lists.add(i);
                System.out.println(i);
                if (i == 5){

                    // 让线程b join 过来
                    try {
                        b.start();
                        b.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
//
//                try {
//                    TimeUnit.SECONDS.sleep(1);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
            }
        });

        a.start();
    }

}
