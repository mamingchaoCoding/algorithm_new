package com.mamingchao.concurrent.threads_communication3;

import java.util.ArrayList;
import java.util.List;

/**
 * 通过volatile 保证nofify 字段 thread b及时可见
 *
 * 基于 volatile 关键字来实现线程间相互通信是使用共享内存的思想，大致意思就是多个线程同时监听一个变量，
 * 当这个变量发生变化的时候 ，线程能够感知并执行相应的业务。这也是最简单的一种实现方式
 *
 * 2020/10/29 这个代码也是有问题的。只要是a在5的时候不等B，继续执行，就不能保证b 刚好在a 5的时候，b执行了
 * Created by mamingchao on 2020/9/19.
 */
public class WithVolatile {

    static volatile boolean notify = false;

    static List<Integer> lists = new ArrayList<>();

    public static void main(String[] args) {

        Thread a = new Thread(() -> {

            for (int i = 0; i < 10; i++) {
                lists.add(i);
                System.out.println(i);
                if (i == 5){
                    notify = true;
                }
            }
        });

        Thread b = new Thread( ()->{
           while (true) {
               if (notify) {
                   System.out.println("Thread b has receive the notification");
                   break;
               }
           }
        });


        a.start();
        b.start();
    }

}
