package com.mamingchao.concurrent.threads_communication3;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 这个实现是依托与 Semaphore 的 思路
 *
 * 看起来比 Semaphore 更简洁
 *
 * 同时也是必须保证a先运行
 *
 *
 * 2020/10/29 review 这个代码，这个代码是有问题的
 * 因为 a unlock 后，并没有等待b执行完，而是继续执行，
 * Created by mamingchao on 2020/9/19.
 */
public class WithReentrantLock {

    static ReentrantLock lock = new ReentrantLock();

    static List<Integer> lists = new ArrayList<>();

    public static void main(String[] args) {

        Thread a = new Thread(() -> {
            lock.lock();

            for (int i = 0; i < 10; i++) {
                lists.add(i);
                System.out.println(i);
                if (i == 5){
                    lock.unlock();
                }
            }
        });

        Thread b = new Thread( ()->{

            lock.lock();
            System.out.println("Thread b has receive the notification");

            lock.unlock();
        });


        //必须保证 a先运行
        a.start();
        b.start();
    }

}
