package com.mamingchao.concurrent.threads_communication3;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * jdk1.5之后在java.util.concurrent包下提供了很多并发编程相关的工具类，简化了我们的并发编程代码的书写，
 * ***CountDownLatch***基于AQS框架，相当于也是维护了一个线程间共享变量state
 *
 * 这个 当把sleep 注释掉的时候，还是有问题，这个需要在研究一下
 *
 * 结论：a 在size==5的时候，确实打开了b的门闩latch，a接着往下走，b也开门往下走
 * a没有在5的时候等待b执行，所以等b打印出来的时候，a可能已经执行到6 或者7 或者8了
 * 可以给a b加一个锁，也可以给a也加一个门闩latch
 *
 * Created by mamingchao on 2020/9/19.
 */
public class WithCountLatchDown {

    static CountDownLatch latch = new CountDownLatch(5);
    static CountDownLatch latch1 = new CountDownLatch(1);

    static List<Integer> lists = new ArrayList<>();

    public static void main(String[] args) {

        Thread a = new Thread(() -> {

            for (int i = 0; i < 10; i++) {
                lists.add(i);
                System.out.println(i);
                latch.countDown();
                if (lists.size() == 5){
                    try {
                        latch1.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
//                try {
//                    TimeUnit.SECONDS.sleep(1);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
            }
        },"Thread a");

        Thread b = new Thread( ()->{
            try {
                latch.await();
                System.out.println("线程B收到通知，开始执行自己的业务...");
                latch1.countDown();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"Thread b");


        a.start();
        b.start();
    }

}
