package com.mamingchao.concurrent.ThreadLocal;

import java.util.concurrent.TimeUnit;

/**
 * Created by mamingchao on 2020/11/3.
 */
public class WithThreadLocal {

    static ThreadLocal<Person> p = new ThreadLocal<>();


    public static void main(String[] args) {
        new Thread(()->{
            p.set(new Person());
            System.out.println(p.get());

            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(p.get());

        }).start();

        new Thread(()->{

            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            p.set(new Person("lisi"));

        }).start();
    }


    static private class Person{
        String name = "zhangsan";

        public Person(){}

        public Person(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }
}
