package com.mamingchao.concurrent.FourReference;

import java.lang.ref.SoftReference;

/**
 * 软引用，只有到内存不够分配的时候，才会被回收
 *
 * 我设置最大最小堆内存 20MB
 *
 *
 * Created by mamingchao on 2020/11/3.
 */
public class MySoftReference {

    public static void main(String[] args) {
        SoftReference<byte[]> m = new SoftReference<>(new byte[1024*1024*10]);
        System.out.println(m.get());
        System.gc();

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(m.get());


//        SoftReference<byte[]> m1 = new SoftReference<>(new byte[1024*1024*15]);
        //再分配一个数组，heap将装不下，这个时候系统会垃圾回收，先回收一次，如果不够，会吧软引用干掉
        //视频里正常回收，我这里 报oom，需要再研究一下为什么
        byte[] m1 = new byte[1024*1024*15];
        System.out.println(m.get());
    }
}
