package com.mamingchao.concurrent.FourReference;

/**
 * 虚引用，在jvm管理的堆外内存 建立对象，回收时候用
 * 当在堆外的对象被回收的时候，会把被回收的对象放在堆内的一个queue里
 * 只是jvm 能在queue内 找到这个被回收的对象
 *
 * 这个只有是jvm的开发，或者NIO的 Direct？
 * 或者自己开发一个 netty 可能会用到这个引用，因为这两个涉及到堆外内存
 *
 * Created by mamingchao on 2020/11/3.
 */
public class MyPhantomReference {
}
