package com.mamingchao.concurrent.Lock.volatile_test.v2;

import java.util.ArrayList;
import java.util.List;

/**
 * 这段代码演示的是，volatile 解决的是 变量线程之间可见性
 * 并不能解决 操作的原子性（count ++ 操作）
 *
 * 因为加不加volatile，结果都不能是100000
 *
 *
 * Created by mamingchao on 2020/10/29.
 */
public class T {
    volatile int count = 0 ;

    void m(){
        for (int i = 0; i < 10000; i++) {
            count ++ ;
        }
    }

    public static void main(String[] args) {
        T t = new T();
        List<Thread> threadList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            threadList.add(new Thread(t::m));
        }

        threadList.forEach( (e) -> {
            e.start();
        });

        //这段代码的意思是 ，等待所有的 线程都结束，再打印
        threadList.forEach( (e) -> {
            try {
                e.join();
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        });


        System.out.println(t.count);

    }

}
