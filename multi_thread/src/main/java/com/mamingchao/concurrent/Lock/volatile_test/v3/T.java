package com.mamingchao.concurrent.Lock.volatile_test.v3;

import java.util.ArrayList;
import java.util.List;

/**
 * v2 版本的问题，m上加 synchronized
 * 所以，volatile 不能 替代 synchronized
 * Created by mamingchao on 2020/10/29.
 */
public class T {
    volatile int count = 0 ;

    synchronized void m(){
        for (int i = 0; i < 10000; i++) {
            count ++ ;
        }
    }

    public static void main(String[] args) {
        T t = new T();
        List<Thread> threadList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            threadList.add(new Thread(t::m));
        }

        threadList.forEach( (e) -> {
            e.start();
        });

        //这段代码的意思是 ，等待所有的 线程都结束，再打印
        threadList.forEach( (e) -> {
            try {
                e.join();
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        });


        System.out.println(t.count);

    }

}
