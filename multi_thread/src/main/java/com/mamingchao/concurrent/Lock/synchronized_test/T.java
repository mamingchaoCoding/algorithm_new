package com.mamingchao.concurrent.Lock.synchronized_test;

import java.util.concurrent.TimeUnit;

/**
 * synchronized 锁对象 里面属性发生变化，不影响
 * 但是如果 锁对象o变成另外一个对象，就变成了另外的一个对象，就不可以了
 * 所以，尽量避免锁对象 变化成另外的对象，一般 给锁对象上面加 final
 * Created by mamingchao on 2020/10/29.
 */
public class T {
    /*final*/ Object o = new Object();
    volatile int count =0;

    void m() {
        synchronized (o) {
            for (int i = 0; i < 10000; i++) {
                count ++ ;
            }
        }
    }

    public static void main(String[] args) {
        T t = new T();
        for (int i = 0; i < 10; i++) {
             new Thread(t::m).start();
             //如果下面注释打开，锁就不好用了
//            if (i ==2) {
//                t.o = new Object();
//            }
        }

        //等所有线程都执行完
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(t.count);
    }
}
