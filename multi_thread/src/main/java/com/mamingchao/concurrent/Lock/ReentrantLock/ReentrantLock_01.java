package com.mamingchao.concurrent.Lock.ReentrantLock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 可重入锁
 * 重入一定是同一个线程，原来是你拿到了锁；然后 你又过来申请锁，发现还是你，那就直接进来吧
 * Created by mamingchao on 2020/10/26.
 */
public class ReentrantLock_01 {
    static int i;
    static ReentrantLock lock = new ReentrantLock();

    static void m(){
        lock.lock();
        i++;
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    public static void main(String[] args) {

        ReentrantLock_01.m();

        new Thread(ReentrantLock_01::m).start();

//        lock.unlock();
    }

}
