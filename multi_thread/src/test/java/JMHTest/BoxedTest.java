package JMHTest;

import org.openjdk.jmh.annotations.*;

/**
 * Created by mamingchao on 2020/12/22.
 */
public class BoxedTest {

    @Benchmark
    @Fork(2)
    @BenchmarkMode({Mode.SampleTime,Mode.AverageTime})
    @Measurement(iterations=2,time = 3)
    @Warmup(iterations=1,time = 3)
    public void testBoxedLong(){

        long sum = 0L;
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            sum +=i;
        }
        System.out.println(sum);
    }


}
