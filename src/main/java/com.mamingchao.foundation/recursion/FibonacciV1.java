package com.mamingchao.foundation.recursion;

import java.util.Scanner;

/**
 * 程序本身的逻辑可以拆分出许多子逻辑，子逻辑于程序逻辑本身一样，这个时候可以用递归
 *
 * 慎用递归，因为递归对资源消耗比较大，处理不好，造成栈溢出
 * 对于可能造成 栈溢出的 优化方式，是把调用递归方法的 语句，放在最后程序一行
 *
 * 当然，如果遇到只能用递归来解决的问题，也只要采用递归，比如 罗列出某个磁盘里的所有目录
 *
 * 递归本身就是一个栈遍历，递归本身就是一个压栈弹栈的操作。（任何可使用递归的地方都可以用迭代来解决？ 我持怀疑态度）
 * 当递归 可以更自然的反应问题，便于程序工作方式的理解，而且对效率没有太多要求的时候，可以用递归
 *
 * 递归在调用层数多的时候，效率要比迭代慢很多
 * 如果要求高性能的话，就要尽量避免使用 递归
 * Created by mamingchao on 2021/3/9.
 */
public class FibonacciV1 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入想要得到的斐波那契数组元素个数：");
        int loop = sc.nextInt();
        for (int i = 1; i <= loop; i++) {
            System.out.print(getNum(i) + "  ");
        }

    }

    private static int getNum(int num) {
        if (num < 1) {
            throw new RuntimeException("输入了非正整数参数，请输入正整数--" + num);
        }

        if (num == 1 || num == 2) {
            return 1;
        } else {
            return getNum( num - 1) + getNum(num - 2);
        }
    }
}
