关于排序算法的稳定性，不是根据数据情况不同，算法的时间复杂度不一样；
对于上面的描述，举个例子：
[7,6,5,4,3,2,1,0] 这样一个数组， 用冒泡做升序排序 和降序排序
比如二分法，用 最后一个元素 0 最为二分的轴，和用4做二分的轴；

这两个例子，虽然用同样的排序算法，但是效率不一样；但是算法的稳定性，说的不是这个

算法的稳定性，说的是，排序过程 不破坏元素的原始顺序

比如[1,3,2,1,3,2,3,1,2]
     0 1 2 3 4 5 6 7 8
排序后
[1,1,1,2,2,2,3,3,3]
 0 3 7 2 5 8 1 4 6(这个index是对应原始数组的顺序的)
 这里 排完顺序，原来排在0位置的1，还在0位置，而没有被挪到 1或者2的位置，这个就是稳定的

 稳定的排序算法的前提条件，是如果两个数对比相等的时候，就不交换位置或者动原来被对比的那个数

 对于基础数据类型的排序，稳定不稳定没有什么影响，比如整型数组；但对于复杂的类型，比如对象，稳定的算法有一个好处；举例子说明：
    比如一个Student对象，有class 和age 两个属性
    我先用age从小到大 最一次排序，在从class 从小到大 最一次排序

    稳定的算法
       每个班级里的人，age 都还是从小到大的； 第二次排序，没有破坏第一次的
   不稳定的算法
        每个班级里的人，age是错乱的、未排好顺序的；第二次排序，破坏了第一次的排序结果

   所以稳定的排序算法，对于一些业务场景，是非常有用的。


 桶排序是稳定的；
