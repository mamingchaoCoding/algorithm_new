package com.mamingchao.foundation.eor;

/**
 * 当乡宦交换的 两个值是不同的内存空间，是没问题的
 * 但如果两个值，是同一个内存空间，a ^ a，那么就会得到0的结果
 * Created by mamingchao on 2020/11/14.
 */
public class swapData {
    public static void main(String[] args) {
        int[] array = new int[]{2,2,5};

        swap(array,0,2);
        swap(array,0,1);


        System.out.println(array[0]);
        System.out.println(array[1]);
        System.out.println(array[2]);


        swap(array,0,0);
        System.out.println(array[0]);
        System.out.println(array[1]);
        System.out.println(array[2]);





    }

    private static void swap(int[] array,int i,int j) {
        array[i]= array[i] ^ array[j];
        array[j]= array[i] ^ array[j];
        array[i]= array[i] ^ array[j];
    }


}
