package com.mamingchao.foundation.eor;

import java.util.Random;

/**
 * 给定一个int 值，返回该值 binary 里面有几个1
 * Created by mamingchao on 2020/11/14.
 */
public class CountInt1Times {
    public static void main(String[] args) {
        int i = 7;

        int count = 0;
        while (i !=0) {
            int rightOne = i & ((~i)+1);
            i ^=rightOne;
            count ++;
        }

        System.out.println(count);


//        for (int i = 0; i < 100; i++) {
//            System.out.println(new Random(1).nextInt());
//        }


    }
}
