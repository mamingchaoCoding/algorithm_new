package com.mamingchao.foundation.eor;

/**
 * 一个数组的，只有一个数出现了奇数次，其他的都出现了偶数次
 * there is a array, only one element in it come out as odd times
 * other elements in it come out even times
 *
 * Created by mamingchao on 2020/11/14.
 */
public class FindOddTimesNum {
    public static void main(String[] args) {
        int[] arr = new int[] {1,1,2,3,4,5,6,7,7,5,6,4,3};

        int result = 0;

        for (int i = 0; i < arr.length; i++) {
            result ^= arr[i];
        }

        System.out.println(result);
    }
}
