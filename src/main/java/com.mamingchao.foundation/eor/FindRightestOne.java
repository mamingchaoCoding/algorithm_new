package com.mamingchao.foundation.eor;

/**
 * 怎么把一个int类型的整数，提取出最右侧的1来（二进制）
 * Created by mamingchao on 2020/11/14.
 */
public class FindRightestOne {
    public static void main(String[] args) {
        int a = 50;

        System.out.println(a &(-a+1));
    }
}
