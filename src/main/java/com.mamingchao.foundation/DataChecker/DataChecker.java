package com.mamingchao.foundation.DataChecker;

import com.mamingchao.foundation.arraysort.quicksort.QuickSort1;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by mamingchao on 2021/3/9.
 */
public class DataChecker {

    private static int[] generateRandomArray(int arrSize){

        int[] array = new int[arrSize];
        for (int i = 0; i < arrSize; i++) {
            array[i] = new Random(arrSize).nextInt();
        }
        return array;
    }

    public static void main(String[] args) {
        int arrSize = 10000;
        int[] array = generateRandomArray(arrSize);
        int[] newArray = Arrays.copyOf(array,array.length);

        Arrays.sort(array);

        long startTime = System.currentTimeMillis();

        //插入排序
//        InsertSort.sort(newArray);
//        InsertSortV1.sort(newArray);
//        BubbleSort.sort(newArray);
//        TimSortUpgrade.sort(newArray);
//        ShellSort.sort(newArray);
        QuickSort1.sort(newArray,0,arrSize -1);
//        InsertSortV2.sort(newArray);
//        InsertSortV3.sort(newArray);
//        DualPovitQuickSort.sort(newArray,0,arrSize -1);
        System.out.println("排序耗时-" + (System.currentTimeMillis() - startTime));

        boolean result = true;
        for (int i = 0; i < array.length; i++) {

            if (array[i] != newArray[i]) {
                result = false;
            }
        }

        System.out.println("对数器 对数结果：" + result);


    }
}
