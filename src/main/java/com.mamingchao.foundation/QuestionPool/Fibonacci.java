package com.mamingchao.foundation.QuestionPool;

import java.util.Scanner;

/**
 *  数组的值由前两个值相加 计算得到，第一个和第二个是1
 *
 * Created by mamingchao on 2021/3/8.
 */
public class Fibonacci {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("请输入想要打印斐波那契数组个数：");
        int num = sc.nextInt();

        if (num < 1 ) {
            System.out.println("请输入正整数!");
        }

        int x = 1;
        int y = 1;
        int z = 0;

        for (int i = 0; i < num; i++) {
            if (i == 0 || i == 1) {
                System.out.print(1 + "\t");
            } else {
                z = x + y;
                x = y;
                y = z;
                System.out.print(z + "\t");
            }
        }
    }
}
