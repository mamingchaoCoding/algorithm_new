package com.mamingchao.foundation.QuestionPool;

/**
 * Created by mlamp on 2024/6/18.
 * 通过递归方式获取数组中的最大值
 * 这个时间复杂度，应该是比 大根堆差点
 * 因为这个 递归，相当于把数组的元素，最后铺在了叶子节点上
 * 而  大根堆，所有节点都是只
 * by 左程云老师
 */
public class GetMaxByRecursion {


    public static void main(String[] args) {
        int[] arr = {1,3,5,7,29,9,4,6,8};


        int L= 0;
        int R = 7;

        int maxNum = recursion(arr, L, R);
        System.out.println(maxNum);

    }

    private static int recursion(int[] arr, int L, int R){
        if (L == R){
            return arr[L];
        }

        int mid = L + ((R-L) >> 1);

        int leftMax = recursion(arr, L, mid);
        int rightMax = recursion(arr, mid + 1, R);

        return Math.max(leftMax, rightMax);

    }
}
