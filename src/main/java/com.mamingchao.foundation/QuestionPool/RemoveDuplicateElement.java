package com.mamingchao.foundation.QuestionPool;

import java.util.Arrays;

/**
 * 删除有序数组中的重复数据
 *
 * 双指针实现
 * Created by mamingchao on 2020/11/14.
 */
public class RemoveDuplicateElement {

    public static void main(String[] args) {
        int[] arr = new int[]{1,1,2,2,2,3,4,4,5};
        int index = removeDuplicates(arr);

//        System.out.println(index);

    }

    private static int removeDuplicates(int[] arr) {
        if (arr.length == 0) return 0;

        int i = 0;
        for (int j = 1; j < arr.length; j++) {
            if (arr[i] != arr[j]) {
                i++;
                arr[i] = arr[j];
            }
        }

        int[] result = Arrays.copyOf(arr,i+1);

        return i+ 1;
    }


}
