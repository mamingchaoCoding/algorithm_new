package com.mamingchao.foundation.QuestionPool;

/**
 * 百钱买百鸡
 *  有公鸡、母鸡、和小鸡，一共100只
 *  公鸡5块钱，母鸡3块钱，小鸡 3个1块钱；一个一百块钱
 *
 *  如果 公鸡是x，母鸡是y，小鸡是z，利用三元一次方程式，就是 x+y+z =100
 *  5x + 3y + z/3 = 100
 *  三元一次方程，如果就两个公式（两个条件） 是计算不出来唯一结果的
 *  程序上，我们可以一个值一个值的试，枚举
 *  x 最大值不能超过20，y最大值不能超过33，z最大值不能超过300
 *
 *  利用三层循环来做
 *
 * Created by mamingchao on 2021/3/8.
 */
public class HundredCoinForHundredChickens {

    public static void main(String[] args) {
        for (int x = 0; x <= 20; x++) {
            for (int y = 0; y <=33; y++) {
                // for 循环条件上 不能加 z%3 == 0  z只能取到0的值
//                for (int z = 0; z <= 300 && z%3 == 0; z++) {
                for (int z = 0; z <= 100 - x- y ; z++) {
                    if ((5*x + 3*y + z/3 == 100) && (x + y + z == 100) && z%3 == 0) {
                        System.out.printf("公鸡数： %d;母鸡数：%d; 小鸡数：%d", x,y,z);
                        System.out.println();
                    }
                }
            }
        }
    }
}
