package com.mamingchao.foundation.GetPrime;

import java.util.ArrayList;

/**
 * 获取指定范围的 质数
 * Created by mamingchao on 2020/12/1.
 */
public class GeneratePrime {

    private static boolean isPrime(int num){
        for (int i = 2; i <= num/2;  i++) {
            if (num % i == 0 )
                return false;
        }
        return true;
    }

    public static ArrayList getPrimeList(int start, int end) {
        ArrayList primes = new ArrayList();
        for (int i = start; i < end; i++) {
            if (isPrime(i))
                    primes.add(i);
        }

        return primes;
    }

    public static void main(String[] args) {

//        System.out.println(isPrime( 3));

        System.out.println(Integer.SIZE);
    }
}
