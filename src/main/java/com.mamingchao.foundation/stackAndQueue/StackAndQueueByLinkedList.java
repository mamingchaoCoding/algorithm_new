package com.mamingchao.foundation.stackAndQueue;

/**
 * 链表实现 栈和队列
 * Created by mamingchao on 2020/11/14.
 */
public class StackAndQueueByLinkedList{

    private class Node<T>{
        T value;
        Node<T> pre;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }

    public class MyLinkedList<T> {
        Node<T>  head;
        Node<T>  tail;
        int size;

        public void addFromTop(T value){
            Node<T> node = new Node<T>(value);
            if (size == 0) {
                head = node;
                tail = node;
            } else {
                node.next = head;
                head = node;
            }
            size ++;
        }

        public void addFromBottom(T value){
            Node<T> node = new Node<T>(value);
            if (size == 0) {
                head = node;
                tail = node;
            } else {
                node.pre = tail;
                tail = node;
            }
            size ++;
        }

        public T removeFromTop(){
            if (size == 0 ){
                return null;
            }

            T result = head.value;
            head=head.next;
            head.pre = null;
            size --;

            return result;
        }

        public T removeFromBottom(){
            if (size == 0 ){
                return null;
            }

            T result = tail.value;
            tail=tail.pre;
            tail.next = null;

            size --;
            return result;
        }

        public int size(){
            return size;
        }
    }


    public class MyQueue<T>{

//        public void push
    }


}
