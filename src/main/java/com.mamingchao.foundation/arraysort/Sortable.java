package com.mamingchao.foundation.arraysort;

/**
 * Created by mamingchao on 2021/3/19.
 */
@FunctionalInterface
public interface Sortable {
    void sort(int[] arr,int start,int end);

//    default void swap(int[] arr, int i, int j) {
//        int temp = arr[i];
//        arr[i] = arr[j];
//        arr[j] = temp;
//    }

    // 更高效的叫唤方法
    default void swap(int[] arr, int i, int j) {
        arr[i] = arr[i] ^ arr[j];
        arr[j] = arr[i] ^ arr[j];
        arr[i] = arr[i] ^ arr[j];
    }

    default void printArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
}
