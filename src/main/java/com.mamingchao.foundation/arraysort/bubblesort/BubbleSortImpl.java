package com.mamingchao.foundation.arraysort.bubblesort;

import com.mamingchao.foundation.arraysort.Sortable;

/**
 * Created by mamingchao on 2021/3/19.
 */
public class BubbleSortImpl implements Sortable{
    @Override
    public void sort(int[] arr, int start, int end) {
        // check arg array

        if (arr == null || arr.length <2) {
            return;
        }

        for (int i = arr.length -1; i >0 ; i--) {
            for (int j = 0; j <i; j++) {
                //if the pre element is bigger than the successor, then swap
                if(arr[j] >  arr[j+1])  {
                    swap(arr,j,j+1);
                }
            }
        }
    }
}
