package com.mamingchao.foundation.arraysort.selectsort;

/**
 * Created by mamingchao on 2020/11/13.
 */
public class SelectSort {
    public static void sort(int[] arr) {
        // check arg array

        if (arr == null || arr.length <2) {
            return;
        }

        //0-n
        //1-n
        //2-n
        for (int i = 0; i < arr.length -1; i++) {

            int minIndex = i;
            for (int j = i + 1; j < arr.length ; j++) {
                minIndex = arr[j] < arr[minIndex] ? j : minIndex;
            }
            swap(arr,minIndex,i);
        }
    }

    private static void swap(int[] arr, int minIndex, int i) {
        int minimum = arr[minIndex];
        arr[minIndex] = arr[i] ;
        arr[i] = minimum;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{5,3,6,1,8,2,1,0};

        sort(arr);
        for (int i = 0; i < arr.length; i++) {

            System.out.print(arr[i] + "\t");
        }
    }
}
