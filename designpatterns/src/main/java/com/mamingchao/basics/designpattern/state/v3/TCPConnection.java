package com.mamingchao.basics.designpattern.state.v3;

/**
 * Created by mamingchao on 2020/10/22.
 */
public class TCPConnection{
    TCPState state;

    public TCPConnection(TCPState state) {
        this.state = state;
    }


    void open() {
        state.open();
    }

    void close() {
        state.close();
    }

    void acknowledge() {
        state.acknowledge();
    }
}
