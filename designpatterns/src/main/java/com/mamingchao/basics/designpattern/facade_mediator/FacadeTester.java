package com.mamingchao.basics.designpattern.facade_mediator;

/**
 * Created by mamingchao on 2020/10/13.
 */
public class FacadeTester {
    public static void main(String[] args) {
        Facade f = new Facade();
        f.reception("mamingchang","muxiaolin");
    }
}
