package com.mamingchao.basics.designpattern.iterator.v1;

/**
 * Created by mamingchao on 2020/10/19.
 */
public class ArrayList_ implements Collection_{

    int index;

    Object[] objects = new Object[10];

    public ArrayList_() {

    }

    @Override
    public void add(Object o) {
        if (index >= objects.length) {
            Object[] newObjects = new Object[objects.length*2];
            System.arraycopy(objects,0,newObjects,0,objects.length);
            objects = newObjects;
        }

        objects[index] = o;
        index ++;
    }

    @Override
    public int size() {
        return index;
    }

    @Override
    public void remove(Object o) {
        for (int i = 0; i < index; i++) {
            if (o.equals(objects[i])) {
                System.arraycopy(objects,i+1,objects,i,index-i);
                index --;
            }
        }
    }

    @Override
    public Object get(int index) {
        return objects[index];
    }

    @Override
    public Iterator_ iterator() {
        return new ArrayListIterator_();
    }



    class ArrayListIterator_ implements Iterator_{

        int currentIndex;

        @Override
        public boolean hasNext() {
            if (currentIndex < index) return true;
            return false;
        }

        @Override
        public Object next() {
            Object result =  objects[currentIndex];
            currentIndex ++ ;
            return result;
        }
    }
}



