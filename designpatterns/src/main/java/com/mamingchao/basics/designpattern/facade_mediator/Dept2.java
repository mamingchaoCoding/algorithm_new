package com.mamingchao.basics.designpattern.facade_mediator;

/**
 * 房产交易大厅
 * Created by mamingchao on 2020/10/13.
 */
public class Dept2 {

    public void dealWithHouseProperty(){
        System.out.println("Your house property has been processed");
    }
}
