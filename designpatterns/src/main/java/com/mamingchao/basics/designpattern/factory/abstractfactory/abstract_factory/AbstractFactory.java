package com.mamingchao.basics.designpattern.factory.abstractfactory.abstract_factory;

import com.mamingchao.basics.designpattern.factory.abstractfactory.abstract_product.Food;
import com.mamingchao.basics.designpattern.factory.abstractfactory.abstract_product.Vehicle;
import com.mamingchao.basics.designpattern.factory.abstractfactory.abstract_product.Weapon;

/**
 * Created by mamingchao on 2020/10/12.
 */
public abstract class AbstractFactory {
    public abstract Food createFood();
    public abstract Vehicle createVehicle();
    public abstract Weapon createWeapon();
}
