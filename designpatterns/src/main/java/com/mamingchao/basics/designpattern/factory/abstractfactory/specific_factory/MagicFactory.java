package com.mamingchao.basics.designpattern.factory.abstractfactory.specific_factory;

import com.mamingchao.basics.designpattern.factory.abstractfactory.abstract_factory.AbstractFactory;
import com.mamingchao.basics.designpattern.factory.abstractfactory.abstract_product.Food;
import com.mamingchao.basics.designpattern.factory.abstractfactory.abstract_product.Vehicle;
import com.mamingchao.basics.designpattern.factory.abstractfactory.abstract_product.Weapon;
import com.mamingchao.basics.designpattern.factory.abstractfactory.specific_product.Broom;
import com.mamingchao.basics.designpattern.factory.abstractfactory.specific_product.MagicStick;
import com.mamingchao.basics.designpattern.factory.abstractfactory.specific_product.Mushroom;

/**
 * Created by mamingchao on 2020/10/12.
 */
public class MagicFactory extends AbstractFactory{
    @Override
    public Food createFood() {
        return new Mushroom();
    }

    @Override
    public Vehicle createVehicle() {
        return new Broom();
    }

    @Override
    public Weapon createWeapon() {
        return new MagicStick();
    }
}
