package com.mamingchao.basics.designpattern.prototype.v3;

/**
 * 基于v2，如果Person name 是 new String，会怎么样呢？
 *
 * 当p name = new String("马明超")，会在栈里生成一个新的String对象，指向String 常量池 "马明超"
 * 同时p1 的name 引用也指向 上文说的栈里面的那个 新生成的String 对象
 *
 * 当p1 的name发生变化的时候，p 和 p1 里面的那么，不再 指向相同的String对象了，也不再指向相同的String 常量
 *
 * Created by mamingchao on 2020/10/21.
 */
public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {
        Person p = new Person();
        Person p1 = (Person)p.clone();

        System.out.println( p == p1);
        System.out.println( p.name == p1.name);
        System.out.println("-------------------------");

        System.out.println(p);
        System.out.println(p1);

        p1.name = "xiaolin";

        System.out.println("-------------------------");

        System.out.println( p == p1);
        System.out.println( p.name == p1.name);

        System.out.println("-------------------------");

        System.out.println(p);
        System.out.println(p1);

    }
}


class Person implements Cloneable{
    String name = new String("马明超");

    @Override
    public  Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }
}

