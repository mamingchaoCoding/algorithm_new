package com.mamingchao.basics.designpattern.singleton;

/**
 * 不仅解决线程同步问题，还可以防止反序列化
 *
 * 这里的反序列号，是【反序列化不成枚举】？？
 *
 * Created by mamingchao on 2020/10/9.
 */
public enum EnumWay7 {
    INSTANCE;

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(()->{
//                System.out.println(LazyMan.getInstance());
                System.out.println(EnumWay7.INSTANCE.hashCode());
            }).start();
        }
    }


}
