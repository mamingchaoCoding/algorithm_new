package com.mamingchao.basics.designpattern.state.v1;

/**
 * 美眉 有两种状态，高兴，伤心
 *
 * 有三种动作，哭，笑，和说话
 * 哭的时候，如果是高兴状态，就是喜极而泣，伤心的状态，就是伤心的哭；
 * 笑的时候，对应的就是开心的笑，苦笑或者强颜欢笑，
 * 说话的时候，对应的就是 笑着说，哭着说，伤心的说
 *
 * 这种 写法挺受用的，但是当如果需要增加新的状态的时候，代码改动量 很大
 * Created by mamingchao on 2020/10/22.
 */
public class MM {

    enum MMState{
        HAPPY,SAD
    }

    private MMState state;

    public MM(MMState state) {
        this.state = state;
    }

    void cry(){
        switch (state){
            case HAPPY:
                System.out.println("pretty girl are crying happily");
                break;
            case SAD:
                System.out.println("pretty girl are crying sadly");
                break;
            default:
                System.out.println("pretty girl are crying with no state");
                break;
        }
    }

    void smile(){
        switch (state){
            case HAPPY:
                System.out.println("pretty girl are smiling happily");
                break;
            case SAD:
                System.out.println("pretty girl are smiling sadly");
                break;
            default:
                System.out.println("pretty girl are smiling with no state");
                break;
        }
    }

    void say(){
        switch (state){
            case HAPPY:
                System.out.println("pretty girl are saying happily");
                break;
            case SAD:
                System.out.println("pretty girl are saying sadly");
                break;
            default:
                System.out.println("pretty girl are saying with no state");
                break;
        }
    }

}
