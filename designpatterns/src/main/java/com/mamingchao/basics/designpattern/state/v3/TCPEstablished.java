package com.mamingchao.basics.designpattern.state.v3;

/**
 * Created by mamingchao on 2020/10/22.
 */
public class TCPEstablished extends TCPState {
    @Override
    public void open() {

    }

    @Override
    public void close() {

    }

    @Override
    public void acknowledge() {

    }
}
