package com.mamingchao.basics.designpattern.proxy.spring_aop;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by mamingchao on 2020/10/19.
 */
public class Tank {

    public void move() {
        System.out.println("Tank is moving");

        try {
            TimeUnit.SECONDS.sleep(new Random(10).nextInt());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
