package com.mamingchao.basics.designpattern.state.v2;

/**
 * Created by mamingchao on 2020/10/22.
 */
public class MM {
    String name;

    MMState state;

    public MM(String name, MMState state) {
        this.name = name;
        this.state = state;
    }

    void cry(){
        state.cry(name);
    }

    void smile(){
        state.smile(name);
    }

    void say() {
        state.say(name);
    }
}
