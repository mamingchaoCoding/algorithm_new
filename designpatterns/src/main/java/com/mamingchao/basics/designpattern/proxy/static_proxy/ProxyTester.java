package com.mamingchao.basics.designpattern.proxy.static_proxy;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by mamingchao on 2020/10/16.
 */
public class ProxyTester {

    public static void main(String[] args) {
        Tank t = new Tank();

        new TimeProxy(new LogProxy(t)).move();
    }
}

interface Movable{
    void move();
}

class Tank implements Movable{

    @Override
    public void move() {
        System.out.println("Tank is moving");

        try {
            TimeUnit.SECONDS.sleep(new Random(10).nextInt());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}


class LogProxy implements Movable{
    Movable m;
    public LogProxy(Movable m) {
        this.m = m;
    }
    @Override
    public void move() {
        System.out.println("Start  logging....");
        m.move();
        System.out.println("End  logging....");
    }
}

class TimeProxy implements Movable{
    Movable m;

    public TimeProxy(Movable m) {
        this.m = m;
    }

    @Override
    public void move() {
        Long startTime = System.currentTimeMillis();
        System.out.println(startTime);
        m.move();
        Long endTime = System.currentTimeMillis();
        System.out.println(endTime);
        System.out.println("The move method has cost millis seconds--" + (endTime-startTime));
    }
}
