package com.mamingchao.basics.designpattern.decorator.a_little_try;

/**
 * Created by mamingchao on 2020/10/13.
 */
public class Changer implements Morrigan{

    Morrigan m;

    public Changer(Morrigan m) {
        this.m = m;
    }

    @Override
    public void display() {
        m.display();
    }
}
