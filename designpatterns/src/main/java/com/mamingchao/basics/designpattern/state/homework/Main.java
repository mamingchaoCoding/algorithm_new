package com.mamingchao.basics.designpattern.state.homework;

/**
 * Created by mamingchao on 2020/10/23.
 */
public class Main {
    public static void main(String[] args) {
        Car car = new Car(new OpenedDoorCar());
        car.run();
        car.stop();
        car.openDoor();
        car.closeDoor();
    }
}
