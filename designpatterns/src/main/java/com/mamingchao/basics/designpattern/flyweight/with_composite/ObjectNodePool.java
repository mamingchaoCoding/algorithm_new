package com.mamingchao.basics.designpattern.flyweight.with_composite;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by mamingchao on 2020/10/23.
 */
public class ObjectNodePool {
    static List<ObjectNode> list = new ArrayList<>();

    final static int MAX_POOL_SIZE = 50;

    static void add(ObjectNode node) {
        if (list.size() >= MAX_POOL_SIZE) {
            System.out.println("Pool size is up to upper limit");
            return;
        }
        list.add(node);
    }

    static ObjectNode poll(){
        int index = new Random().nextInt(list.size());
        ObjectNode node = list.get(index);
        node.isUsing = true;
        return node;
    }

    static int size(){
        return list.size();
    }
}
