package com.mamingchao.basics.designpattern.flyweight;

/**
 * String 用到的就是享元模式
 * 那么我理解 int 等都是这样
 *
 * Created by mamingchao on 2020/10/15.
 */
public class FlyweightInString {

    public static void main(String[] args) {
        String a = "abc";
        String b = "abc";
        String c = new String("abc");
        String d = new String("abc");

        System.out.println(a == b);
        System.out.println(a == c);
        System.out.println(a == c.intern());
        System.out.println(c == d);
        System.out.println(c.intern() == b.intern());
    }
}
