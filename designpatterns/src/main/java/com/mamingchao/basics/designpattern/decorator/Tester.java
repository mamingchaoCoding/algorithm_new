package com.mamingchao.basics.designpattern.decorator;

import com.mamingchao.basics.designpattern.decorator.democode.Plane;
import com.mamingchao.basics.designpattern.decorator.democode.TailDecorator;

import java.io.IOException;

/**
 * Created by mamingchao on 2020/10/12.
 */
public class Tester {

    public static void main(String[] args) throws IOException {

        new TailDecorator(new Plane()).display();

//        Morrigan originalMorrigan = new OriginalMorrigan();
//        originalMorrigan.display();
//
//        Changer changer = new Succubus(originalMorrigan);
//        changer.display();
//
//        Changer changer1 = new Girl(originalMorrigan);
//        changer1.display();

    }

}
