package com.mamingchao.basics.designpattern.strategy.comparable;

/**
 * 任何一个对象想 实现对比方法的话，都可以 implements java.lang.Comparable 接口
 *
 * 这个接口也是 函数式接口
 *
 * FunctionalInterface
 *
 * 只有一个抽象方法，不写这个注解也成
 *
 * 不带范型，默认就是Object
 *
 * Created by mamingchao on 2020/9/29.
 */
public class ObjectWannaCompare implements Comparable{
    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
