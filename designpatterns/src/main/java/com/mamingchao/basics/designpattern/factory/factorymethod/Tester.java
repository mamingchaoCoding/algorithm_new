package com.mamingchao.basics.designpattern.factory.factorymethod;

/**
 * Created by mamingchao on 2020/10/12.
 */
public class Tester {
    public static void main(String[] args) {
        Factory f = new ModernFactory();
        f.getMoveable().go();

        Factory f1 = new MagicFactory();
        f1.getMoveable().go();
    }
}
