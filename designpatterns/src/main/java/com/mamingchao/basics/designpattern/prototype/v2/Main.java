package com.mamingchao.basics.designpattern.prototype.v2;

/**
 * 这里是深度clone
 * 同时 Person 的name属性 是 String
 * String 有自己的常量池，这部分在设计模式的 FlyWeight 享元模式那里讲过
 * 所以clone完，p和p1都指向 String 常量池里的 "马明超"
 * 当变化完后，p 指向 String 常量池里的 "马明超"，p1 指向String 常量池里的 "xiaolin"
 *
 * Created by mamingchao on 2020/10/21.
 */
public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {
        Person p = new Person();
        Person p1 = p.clone();

        System.out.println( p == p1);
        System.out.println( p.age == p1.age);
        System.out.println( p.name == p1.name);
        System.out.println( p.location == p1.location);
        System.out.println("-------------------------");

        System.out.println(p);
        System.out.println(p1);

        p1.age= 10;
        p1.name = "xiaolin";
        p1.location.roomNo = 100;

        System.out.println("-------------------------");

        System.out.println( p == p1);
        System.out.println( p.age == p1.age);
        System.out.println( p.name == p1.name);
        System.out.println( p.location == p1.location);

        System.out.println("-------------------------");

        System.out.println(p);
        System.out.println(p1);

    }
}


class Person implements Cloneable{
    String name = "马明超";
    int age = 18;
    Location location = new Location();

    @Override
    public  Person clone() throws CloneNotSupportedException {
        Person p = (Person) super.clone();
        p.location = (Location) p.location.clone();
        return p ;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", location=" + location +
                '}';
    }
}

class Location implements Cloneable{
    String provice = "辽宁";
    int roomNo = 1904;

    @Override
    public  Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "Location{" +
                "provice='" + provice + '\'' +
                ", roomNo=" + roomNo +
                '}';
    }
}
