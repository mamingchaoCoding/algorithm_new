package com.mamingchao.basics.designpattern.state.v3;

/**
 * Created by mamingchao on 2020/10/22.
 */
public abstract class TCPState {
    abstract void open();
    abstract void close();
    abstract void acknowledge();

}
