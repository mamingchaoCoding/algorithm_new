package com.mamingchao.basics.designpattern.state.homework;

/**
 * Created by mamingchao on 2020/10/23.
 */
public class StopedCar implements CarAction {
    @Override
    public void openTheDoor() {
        System.out.println("Ok, the car door is opened!");
    }

    @Override
    public void closeTheDoor() {
        System.out.println("Ok, the car door is closed!");

    }

    @Override
    public void runTheCar() {
        System.out.println("Yeah hoo, the car is running!");
    }

    @Override
    public void stopTheCar() {
        System.out.println("Ooh, you can't do that! Your car is already stopped, you can't stop it again!");
    }
}
