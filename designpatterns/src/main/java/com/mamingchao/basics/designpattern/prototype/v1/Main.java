package com.mamingchao.basics.designpattern.prototype.v1;

import java.util.Arrays;

/**
 * 需要实现 标志性接口 Cloneable
 * 虽然 Cloneable 是空接口，但是如果不实现，编译器检查会通过，编译报错
 *
 * 然后需要重载 Object 里面的clone 方法
 * 因为 Object 的 clone 方法是protected，直接访问不到。在重载方法里 调用一下 super.clone()
 *
 * 这里是浅度clone
 *
 * Created by mamingchao on 2020/10/21.
 */
public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {
        Person p = new Person();
        Person p1 = p.clone();

        System.out.println( p == p1);
        System.out.println(p1.equals(p));
        System.out.println(p1.getClass() == p.getClass());
        System.out.println( p.elements == p1.elements);
        System.out.println( p.age == p1.age);
        System.out.println( p.location == p1.location);
        System.out.println("-------------------------");

        System.out.println(p);
        System.out.println(p1);

        p1.age= 10;
        p1.location.roomNo = 100;
        p1.elements[3] = 6;

        System.out.println("-------------------------");

        System.out.println( p == p1);
        System.out.println(p1.equals(p));
        System.out.println(p1.getClass() == p.getClass());
        System.out.println( p.elements == p1.elements);
        System.out.println(Arrays.deepToString(p.elements));
        System.out.println( p.age == p1.age);
        System.out.println( p.location == p1.location);

        System.out.println("-------------------------");

        System.out.println(p);
        System.out.println(p1);
    }
}



class Person implements Cloneable{
    Object[] elements = new Object[]{2,3,4,5};
    int age = 18;
    Location location = new Location();

    @Override
    public  Person clone() throws CloneNotSupportedException {
        return (Person)super.clone();
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", location=" + location +
                '}';
    }
}

class Location{
    String provice = "辽宁";
    int roomNo = 1904;

    @Override
    public String toString() {
        return "Location{" +
                "provice='" + provice + '\'' +
                ", roomNo=" + roomNo +
                '}';
    }
}
