package com.mamingchao.basics.designpattern.state.v1;

/**
 * Created by mamingchao on 2020/10/22.
 */
public class Main {
    public static void main(String[] args) {
        MM mm = new MM(MM.MMState.HAPPY);

        mm.cry();

    }
}
