package com.mamingchao.basics.designpattern.factory.abstractfactory.specific_product;

import com.mamingchao.basics.designpattern.factory.abstractfactory.abstract_product.Vehicle;

/**
 * Created by mamingchao on 2020/10/12.
 */
public class Car extends Vehicle{
    @Override
    public void go() {
        System.out.println("wuwuwu");
    }
}
