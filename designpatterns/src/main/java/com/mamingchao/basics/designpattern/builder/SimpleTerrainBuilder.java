package com.mamingchao.basics.designpattern.builder;

/**
 * Created by mamingchao on 2020/10/20.
 */
public class SimpleTerrainBuilder implements TettainBuilder {
    Terrain terrain = new Terrain();

    @Override
    public TettainBuilder buildWall() {
        terrain.wall = new Wall(10,10,20,30);
        return this;
    }

    @Override
    public TettainBuilder buildFort() {
        return this;
    }

    @Override
    public TettainBuilder buildMine() {
        return this;
    }

    @Override
    public Terrain build() {
        return terrain;
    }
}
