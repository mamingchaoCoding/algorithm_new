package com.mamingchao.basics.designpattern.iterator.v2;

/**
 * Created by mamingchao on 2020/10/19.
 */
public class Main {
    public static void main(String[] args) {

//        ArrayList_ list = new ArrayList_();
        LinkedList_ list = new LinkedList_();
        for (int i = 0; i < 15; i++) {
            list.add("S" + i);
        }

        System.out.println(list);
        System.out.println(list.size());
        list.remove("S10");
        System.out.println(list);
        System.out.println(list.size());
    }
}
