package com.mamingchao.basics.designpattern.iterator.v1;

/**
 * Created by mamingchao on 2020/10/19.
 */
public interface Iterator_ {
    boolean hasNext();

    Object next();
}
