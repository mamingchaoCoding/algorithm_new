package com.mamingchao.basics.designpattern.command;

/**
 * Created by mamingchao on 2020/10/21.
 */
public interface Command {
    void exec();
    void undo();
}


