package com.mamingchao.basics.designpattern.factory.abstractfactory;

import com.mamingchao.basics.designpattern.factory.abstractfactory.abstract_factory.AbstractFactory;
import com.mamingchao.basics.designpattern.factory.abstractfactory.abstract_product.Food;
import com.mamingchao.basics.designpattern.factory.abstractfactory.abstract_product.Vehicle;
import com.mamingchao.basics.designpattern.factory.abstractfactory.abstract_product.Weapon;
import com.mamingchao.basics.designpattern.factory.abstractfactory.specific_factory.ModernFactory;

/**
 * Created by mamingchao on 2020/10/12.
 */
public class Tester {

    public static void main(String[] args) {
        AbstractFactory f = new ModernFactory();
        Food food = f.createFood();
        food.eat();

        Weapon w= f.createWeapon();
        w.shoot();

        Vehicle v = f.createVehicle();
        v.go();
    }
}
