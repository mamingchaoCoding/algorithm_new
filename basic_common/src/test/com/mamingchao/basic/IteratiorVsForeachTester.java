package com.mamingchao.basic;

import org.openjdk.jmh.annotations.*;

/**
 * Created by mamingchao on 2020/12/29.
 */
public class IteratiorVsForeachTester {

//    @Benchmark
//    @Fork(2)
//    @BenchmarkMode({Mode.SampleTime,Mode.AverageTime})
//    @Measurement(iterations=2,time = 3)
//    @Warmup(iterations=1,time = 3)
//    public void testIterator() {
//        IteratorVsForeach.testForIterator();
//    }

//    @Benchmark
//    @Fork(2)
//    @BenchmarkMode({Mode.SampleTime,Mode.AverageTime})
//    @Measurement(iterations=2,time = 3)
//    @Warmup(iterations=1,time = 3)
//    public void testStreamForeach() {
//        IteratorVsForeach.testForStreamForEach();
//    }

    //parallel stream 还没用明白，后续 关注下
//    @Benchmark
//    @Fork(2)
//    @BenchmarkMode({Mode.SampleTime,Mode.AverageTime})
//    @Measurement(iterations=2,time = 3)
//    @Warmup(iterations=1,time = 3)
//    public void testParallelStreamForeach() {
//        IteratorVsForeach.testForParallelStreamForEach();
//    }

    @Benchmark
    @Fork(2)
    @BenchmarkMode({Mode.SampleTime,Mode.AverageTime})
    @Measurement(iterations=2,time = 3)
    @Warmup(iterations=1,time = 3)
    public void testForForLoop() {
        IteratorVsForeach.testForForLoop();
    }
}
