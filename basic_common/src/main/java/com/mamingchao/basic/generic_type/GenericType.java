package com.mamingchao.basic.generic_type;

/**
 * Created by mamingchao on 2021/1/3.
 */
public interface GenericType<T> {

    void setValue(T t);
    T getValue();
}
