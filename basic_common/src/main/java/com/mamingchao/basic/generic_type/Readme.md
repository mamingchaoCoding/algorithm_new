###请不要使用原生态类型
	* 原生态类型是java 1.5 范型出现，为类兼容原来的程序而做的兼容； List 就是 List<String> 的原生态类型
	* List 与 List<Object> 不同在于 List<String> 可以传入 List 类型的 形参，但是 不能调用带 List<Object> 类型形参的 方法
	* 无限通配符 List<?> 与原生态类型List有什么区别呢？无限通配符是安全的，原生态类型是不安全的。比如Collection<?>,第一个元素add进去的是Integer，第二个元素再add String，就会报错；而原生态类型则不会。
