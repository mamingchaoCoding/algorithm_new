package com.mamingchao.basic.final_keyword;

/**
 * final 修饰的方法 只能防止被重写(Override)，对重载（Overload）没影响
 *
 * Created by mamingchao on 2020/12/18.
 */
public class FinalTest {
    final void show(){
        System.out.println("show method");
    }


    void show(String content){
        System.out.println(content);
    }
}

class SubClass extends  FinalTest {
    //这个报错
//    void show(){
//
//    }

    void show(String content){

    }
}
