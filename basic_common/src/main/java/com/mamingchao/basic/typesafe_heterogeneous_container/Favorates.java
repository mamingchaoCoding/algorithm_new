package com.mamingchao.basic.typesafe_heterogeneous_container;

import java.util.HashMap;
import java.util.Map;

/**
 * 范型实现 类型安全的异构容器
 * 因为 常规的设置了参数类型的容器，都有固定数目的类型参数
 * 异构容器 可以 多多种类的，多种类型参数
 * Created by mamingchao on 2021/1/4.
 */
public class Favorates {
    private static Map<Class<?>,Object> favorates = new HashMap<>();

    public static  <T>  void put(Class<T> type, T instance){
        favorates.put(type,type.cast(instance));
    }

    public static <T> T getFavorate(Class<T> type){
        return type.cast(favorates.get(type));
    }


    public static void main(String[] args) {
        Favorates.put(String.class,"12345");
        Favorates.put(Integer.class,123445);

        String arg1 = Favorates.getFavorate(String.class);
        Integer arg2 = Favorates.getFavorate(Integer.class);

        System.out.printf("arg1 - %s; arg2 - %s",arg1,arg2);

    }

}
