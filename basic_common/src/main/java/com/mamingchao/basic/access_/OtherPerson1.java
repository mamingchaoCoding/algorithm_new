package com.mamingchao.basic.access_;

/**
 * Created by mamingchao on 2020/12/28.
 */
public class OtherPerson1 {


    public static void main(String[] args) {
        OtherPerson1 s = new OtherPerson1();
        Parent p = new Parent();

        Parent.PackagePrivate pp = p.new PackagePrivate();
        pp.defaultAge = 19;
        pp.defaultName = "";

        Parent.ProtectedClass pc1 = p.new ProtectedClass();
        pc1.protectedAge = 18;
        pc1.protectedName = "";


    }
}
