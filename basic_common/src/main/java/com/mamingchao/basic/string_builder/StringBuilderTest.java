package com.mamingchao.basic.string_builder;

/**
 * Created by mamingchao on 2020/12/30.
 */
public class StringBuilderTest {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();
        sb.append("abc1234567890defghijklmn");
        sb.append("hqi");
        System.out.println(sb.toString());
    }
}
