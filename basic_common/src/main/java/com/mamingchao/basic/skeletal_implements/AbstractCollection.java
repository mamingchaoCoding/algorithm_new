package com.mamingchao.basic.skeletal_implements;

import java.util.Iterator;

/**
 *
 * Created by mamingchao on 2020/12/31.
 */
public abstract class AbstractCollection<E> implements Collection{

    public abstract Iterator<E> iterator();

    public abstract int size();
}
