package com.mamingchao.basic.enum_;

/**
 * Created by mamingchao on 2021/1/5.
 */
public enum  EnumTest {
    ONE,TWO,THREE,FOUR;

    public int getIndex(){
        return ordinal() + 1;
    }

    public static void main(String[] args) {
        System.out.println(EnumTest.THREE.getIndex());
    }
}
