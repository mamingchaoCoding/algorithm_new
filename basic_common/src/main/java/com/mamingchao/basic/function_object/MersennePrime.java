package com.mamingchao.basic.function_object;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;
import static java.math.BigInteger.ONE;
import static java.util.stream.Collectors.toList;

/**
 * Created by mamingchao on 2021/1/12.
 */
public class MersennePrime {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
//		int i = 0 ;
//		BigInteger a = new BigInteger("41");
//		System.out.println(a.isProbablePrime(10));

//		do {
//			System.out.println(BigInteger.probablePrime(4, new Random()).intValue());
//		}while(++i <10);

//		List cardList = Stream.of(MersennePrime.Suit.values()).flatMap(suit -> Stream.of(MersennePrime.Rank,values()).map(rank -> new Card(suit,rank))).collect(toList());
        String[] words = new String[]{"Hello", "World"};
        List<String[]> a = Arrays.stream(words)
                .map(word -> word.split(""))
                .distinct()
                .collect(toList());
        a.forEach(System.out::print);

    }

    static Stream<BigInteger> primes() {
        return Stream.iterate(ONE, BigInteger::nextProbablePrime);
    }

    enum Suit {
        ONE, TWO, THREE;
    }

    enum Rank {
        ONE, TWO, THREE;
    }

    class Card {
        Suit suit;
        Rank rank;

        Card(Suit suit, Rank rank) {
            this.rank = rank;

        }
    }
}
