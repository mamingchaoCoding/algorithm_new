package com.mamingchao.basic.optional;

import java.util.Optional;

/**
 * Created by mamingchao on 2021/1/29.
 */
public class OptionalTest {
    public static void main(String[] args) {

        Optional<Person> person = getPerson(false);
        System.out.println(person.get());


        Optional<Person> person1 = getPerson(true);
        System.out.println(person1.get());


    }


    private static Optional<Person> getPerson(final boolean flag) {
        //version 1
//        if (flag)
//            return Optional.empty();
        Person p = new Person(18,"少女");
//        return Optional.of(p);

        //version 2
        return Optional.ofNullable(p);
    }

    static class Person{
        int age;
        String name;

        public Person(int age, String name) {
            this.age = age;
            this.name = name;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "age=" + age +
                    ", name='" + name + '\'' +
                    '}';
        }
    }
}
