package com.mamingchao.basic.ExtendVsInterface.interfaces;

import com.mamingchao.basic.ExtendVsInterface.Song;

/**
 * Created by mamingchao on 2020/12/31.
 */
public interface SongWriter {
    Song compose();
}
