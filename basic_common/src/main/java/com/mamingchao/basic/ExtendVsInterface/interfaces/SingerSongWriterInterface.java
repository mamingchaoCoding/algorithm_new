package com.mamingchao.basic.ExtendVsInterface.interfaces;

/**
 * Created by mamingchao on 2020/12/31.
 */
public interface SingerSongWriterInterface extends Singer, SongWriter {
}
