package com.mamingchao.basic.generic_method;

import com.mamingchao.basic.access_1.OtherPerson;

/**
 * 范型方法<T></T>
 *
 *
 * Created by mamingchao on 2021/1/4.
 */
public class GenericSingletonMethod {

    private OtherPerson otherPerson = new OtherPerson();

    public <T> T getObject(){
        return (T)otherPerson;
    }

}
