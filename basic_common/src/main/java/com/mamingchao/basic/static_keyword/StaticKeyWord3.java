package com.mamingchao.basic.static_keyword;

/**
 * statis 静态块
 * 当 只运行 StaticKeyWord3.print();
 * 运行结果显示 初始化顺序 p2 --> p4 --> 静态方法
 * 可见，静态成员变量是在 使用这个类的时候(而不是创建对象)，静态成员变量就初始化了，而且静态成员变量先于静态method 初始化
 *
 * 当 只运行 StaticKeyWord3 p1 = new StaticKeyWord3("初始化"); 的时候
 * p2 --> p4 --> p1 --> p3 --> StaticKeyWord3 构造方法
 *
 * 静态成员变量先初始化，然后非静态的成员变量，最后是本对象的构造方法
 *
 * 当 StaticKeyWord3.print();
 * StaticKeyWord3 p1 = new StaticKeyWord3("初始化");
 * 都打开运行的时候
 * p2 --> p4 --> 静态方法 --> p1 --> p3 --> StaticKeyWord3 构造方法
 *
 * 可见，静态成员变量只初始化一次
 *
 * 当然，类里面有多个static 成员变量，就可以一起放在一个static的大括号里
 * static {
 *
 * }
 * Created by mamingchao on 2020/12/16.
 */
public class StaticKeyWord3 {

    public StaticKeyWord3(String content) {
        System.out.println(content);
    }

    public static void main(String[] args) {

        StaticKeyWord3.print();
        System.out.println("****************");
        StaticKeyWord3 p1 = new StaticKeyWord3("初始化");
    }

    Person p1 = new Person("person 1 has been initialized");
    static Person p2 = new Person("person 2 has been initialized");
    Person p3 = new Person("person 3 has been initialized");
    static Person p4 = new Person("person 4 has been initialized");

    static void print(){
        System.out.println("static method has been initialized");
    }
}

class Person{
    public Person(String content) {
        System.out.println(content);
    }
}