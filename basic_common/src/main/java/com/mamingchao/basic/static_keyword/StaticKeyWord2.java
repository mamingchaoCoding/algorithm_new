package com.mamingchao.basic.static_keyword;

/**
 * 用statis 修饰方法，最重要的作用就是可以StaticKeyWord2.printName()这种调用方法
 * 不用new 出来一个新的对象 ，避免了new对象的资源消耗和对象gc
 * Created by mamingchao on 2020/12/16.
 */
public class StaticKeyWord2 {

    public static void main(String[] args) {
        int count = 0;
        //这是最原始的方法，new 出类对象，然后调用对象里的方法
        StaticKeyWord2 keyWord2 = new StaticKeyWord2();
        keyWord2.printName(++count);

        //类内部 属性、方法之间相互调，肯定没问题
        printName(++ count);

        //如果其他类调用，可以采用如下方式的写法
        StaticKeyWord2.printName(++ count);


    }

    public static void printName(int num){
        System.out.println("print person name! " + num + " times");
    }

}
