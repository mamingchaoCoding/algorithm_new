package com.mamingchao.basic.static_keyword;

/**
 * static 成员变量属于类，所有类成员共享
 * Created by mamingchao on 2020/12/16.
 */
public class StaticKeyWord6 {

    public static void main(String[] args){
        Student a = new Student("xiaoming",20);
        Student b = new Student("lilei",21);

        Teacher t1 = new Teacher("hanmeimei",36);
        Teacher t2 = new Teacher("lidahai",40);

        System.out.println(a.toString() );
        System.out.println(b.toString());
        System.out.println(t1.toString());
        System.out.println(t2.toString());
    }
}

class Student {
    String name;
    int age;
    static String college = "Tsinghua";

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "college='" + college + '\'' +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

class Teacher {
    String name;
    int age;
    static String college = "Peiking";

    public Teacher(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "college='" + college + '\'' +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
