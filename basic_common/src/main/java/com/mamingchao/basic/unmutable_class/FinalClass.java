package com.mamingchao.basic.unmutable_class;

/**
 * final 的类不可变
 * Created by mamingchao on 2020/12/31.
 */
public final class FinalClass {
    public int field1;

    public String field2;

    public FinalClass(int field1,String field2) {
        this.field1 = field1;
        this.field2 = field2;
    }

}
