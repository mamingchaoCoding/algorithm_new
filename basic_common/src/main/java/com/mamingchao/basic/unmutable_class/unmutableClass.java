package com.mamingchao.basic.unmutable_class;

/**
 * Created by mamingchao on 2020/12/31.
 */
public class unmutableClass {
    private int field1;

    private String field2;

    private unmutableClass(int field1, String field2) {
        this.field1 = field1;
        this.field2 = field2;
    }


    public static unmutableClass valueOf(int field1, String field2) {
        return new unmutableClass(field1, field2);
    }


}
