package com.mamingchao.basic.inner_class;

/**
 * 实例内部类
 * 在外部类的静态方法和外部类以外的其他类中，必须通过外部类的实例创建内部类的实例
 *
 * 内部类 可以访问外部类所有 的成员变量和 成员方法
 * 如果有多层嵌套，则内部类可以访问所有外部类的成员。
 *
 * 在外部类的静态方法和外部类以外的其他类中，必须通过外部类的实例创建内部类的实例。
 *
 * 在外部类中不能直接访问内部类的成员，而必须通过内部类的实例去访问。如果类 A 包含内部类 B，类 B 中包含内部类 C，则在类 A 中不能直接访问类 C，而应该通过类 B 的实例去访问类 C。
 *
 * 外部类实例与内部类实例是一对多的关系，也就是说一个内部类实例只对应一个外部类实例，而一个外部类实例则可以对应多个内部类实例。
 *
 * 在实例内部类中不能定义 static 成员，除非同时使用 final 和 static 修饰。
 *
 * Created by mamingchao on 2020/12/17.
 */
public class InstanceInnerClass {
    private int a = 10;
    public String b = "temp";
    static int c = 20;
    final boolean flag = true;

    public String method1() {
        return "实例方法1";
    }

    public static String method2() {
        return "实例方法2";
    }

    private class InnerClass{
        int a1 = a + 10;
        String b1 = b + "inner";
        int c1 = c + 10;
        boolean flag1 = flag;
        // static int d1 = 30;  // 实例内部类不允许声明static 类型
        final static  int e1 = 20; // final static 的可以

        String temp1 = method1();
        String temp2 = method2();
    }

    public static void main(String[] args) {
        InnerClass ic = new InstanceInnerClass().new InnerClass();
        System.out.println(ic.a1);
        System.out.println(ic.b1);
        System.out.println(ic.c1);
        System.out.println(ic.flag1);
        System.out.println(ic.temp1);
        System.out.println(ic.temp2);
    }
}
