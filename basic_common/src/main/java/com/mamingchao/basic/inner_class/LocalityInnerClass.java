package com.mamingchao.basic.inner_class;

/**
 * 局部内部类
 *
 * 局部内部类只在当前方法中有效。
 *
 * 局部内部类中不能定义 static 成员。
 *
 * 在局部内部类中可以访问外部类的所有成员。
 *
 * 局部内部类与局部变量一样，不能使用访问控制修饰符（public、private 和 protected）和 static 修饰符修饰。
 * 局部内部类中还可以包含内部类，但是这些内部类也不能使用访问控制修饰符（public、private 和 protected）和 static 修饰符修饰。
 *
 * Created by mamingchao on 2020/12/17.
 */
public class LocalityInnerClass {
    private int a = 10;
    public String b = "temp";
    static int c = 20;
    final boolean flag = true;

    //局部内部类只在当前方法中有效。以下 语句都报错
//    InnerClass ic = new InnerClass();
//    LocalityInnerClass.InnerClass ic1 = new InnerClass.InnerClass();
//    LocalityInnerClass.InnerClass ic2 = new InnerClass().new InnerClass();

    public void method1() {
        final int f = 90;
        int e = 50;

        class InnerClass{
            int a1 = a + 10;
            String b1 = b + "inner";
            boolean flag1 = flag;

            //直接访问外部类的静态成员变量
            int c1 = c + 10;


            //static int d1 = 30; // 局部内部类不允许有 static 成员变量
            final static  int e1 = 20;

            int f1 = f + 10;
            int e2 = e;

            //直接访问外部类的所有成员方法
            String temp2 = method2();
            String temp3 = method3();
        }

        InnerClass ic = new InnerClass();
        System.out.println(ic.a1);
        System.out.println(ic.b1);
        System.out.println(ic.c1);
        System.out.println(ic.f1);
        System.out.println(ic.e2);
        System.out.println(ic.flag1);
        System.out.println(ic.temp2);
        System.out.println(ic.temp3);

    }

    public static String method2() {
        return "实例方法2";
    }

    public  String method3() {
        return "实例方法3";
    }



    public static void main(String[] args) {
        LocalityInnerClass lic = new LocalityInnerClass();
        lic.method1();
    }
}
