package com.mamingchao.basic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

/**
 * Created by mamingchao on 2020/12/29.
 */
public class IteratorVsForeach {
    static List<String> demoData = new ArrayList<>();
    static {
        int i = 0;
        while (i<1000001) {
            demoData.add(i + "_element");
            i++;
        }
    }

    public static void testForIterator(){
        TreeSet<String> result = new TreeSet<>();

        Iterator<String> iterator = demoData.iterator();
        while (iterator.hasNext()) {
            result.add(iterator.next());
        }
    }

    public static void testForStreamForEach(){
        TreeSet<String> result = new TreeSet<>();

        demoData.stream().forEach(item -> {
            result.add(item);
        });
    }

    public static void testForForLoop(){
        TreeSet<String> result = new TreeSet<>();

        for (int i = 0; i < demoData.size(); i++) {
            result.add(demoData.get(i));
        }
    }

    public static void testForParallelStreamForEach(){
        List<String> result = new ArrayList<>();

        demoData.parallelStream().forEach(item -> {
            result.add(item);
        });
    }

}
