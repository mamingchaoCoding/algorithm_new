package com.mamingchao.basic.access_1;

import com.mamingchao.basic.access_.Parent;

/**
 * Created by mamingchao on 2020/12/28.
 */
public class Son extends Parent {


    public static void main(String[] args) {
        Son s = new Son();
        //protected 访问级别，子类可以访问
        s.age = 1;
        //packageprivate ,不同包不可以访问
        //s.name = "zhangsan";
        System.out.println(s.toString());

        Parent p = new Parent();
        //packageprivate ,不同包不可以访问
//        Parent.PackagePrivate pp = p.new PackagePrivate();

        //protected 的 父类的内部类，也没有access权限
//        Parent.ProtectedClass pc1 = p.new ProtectedClass();

    }
}
