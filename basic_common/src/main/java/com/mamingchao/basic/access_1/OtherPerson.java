package com.mamingchao.basic.access_1;

import com.mamingchao.basic.access_.Parent;

/**
 * Created by mamingchao on 2020/12/28.
 */
public class OtherPerson {


    public static void main(String[] args) {
        OtherPerson s = new OtherPerson();
        Parent p = new Parent();

        //不同包，package private(default) 和 protected 都不可访问
        //可打开注释 看效果
//        Parent.PackagePrivate pp = p.new PackagePrivate();
//        Parent.ProtectedClass pc1 = p.new ProtectedClass();


    }
}
