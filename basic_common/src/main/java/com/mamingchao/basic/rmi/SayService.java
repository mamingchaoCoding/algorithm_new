package com.mamingchao.basic.rmi;

import java.rmi.Remote;

/**
 * Created by mamingchao on 2021/1/23.
 */
public interface SayService extends Remote{
    String say(String message) throws Exception;
}
