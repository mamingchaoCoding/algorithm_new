package individual.mamingchao.scratch_story.common;

/**
 * Created by mamingchao on 2020/12/25.
 */
public enum MaterialType {
    CHARACTER, //人物造型图片
    BGIMAGE, //章节背景图片
    ITEMS, // 其他道具
    GENERIC, //一般通用材料
    SOUND // 声音
}
