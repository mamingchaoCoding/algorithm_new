package individual.mamingchao.scratch_story.common;

/**
 * Created by mamingchao on 2020/12/30.
 */
public enum CodeMsg {
    SUCCESS(200,"success"),
    FAILED(-1,"business failed"),
    NO_AUTH(401,"incorrect account or password"),
    FORBIDDEN(403,"access reject"),
    INTERNAL_SERVER_ERR(500, "internal server error occurred");

    private int code;
    private String msg;

    CodeMsg(int code,String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
