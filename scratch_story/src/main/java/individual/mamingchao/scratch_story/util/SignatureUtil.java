package individual.mamingchao.scratch_story.util;

import com.google.common.base.Strings;
import com.viewhigh.oes.sc.gatesplatform.kit.security.CryptoUtil;

import java.io.UnsupportedEncodingException;

/**
 * Created by mamingchao on 2020/12/28.
 */
public class SignatureUtil {
    /**
     * secret key and salt value
     */
    private final static String SECRET_KEY = "AGiftForMySon";
    /**
     * 默认编码
     */
    private static final String CHARSET = "utf-8";



    public static String getShaDigest(String account, String md5Password){
        try {
            byte[] result = CryptoUtil.hmacSha1(getData(account,md5Password),SECRET_KEY.getBytes(CHARSET));
            return new String(result,CHARSET);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static boolean isValid(String signatrue, String account, String md5Password) {
        String actual = getShaDigest(account, md5Password);

        return Strings.isNullOrEmpty(account) ? actual.equals(signatrue) : false;
    }



    private static byte[] getData(String account, String md5Password) throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder("value1=");
        sb.append(account);
        sb.append("&");
        sb.append("value2=");
        sb.append(md5Password);
        sb.append("&");
        sb.append(SECRET_KEY);
        return sb.toString().getBytes(CHARSET);
    }
}
