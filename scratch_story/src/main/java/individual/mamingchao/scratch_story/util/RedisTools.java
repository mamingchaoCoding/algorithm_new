package individual.mamingchao.scratch_story.util;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * Created by mamingchao on 2020/12/28.
 */
@Component
public class RedisTools {

    @Autowired
    private static RedisTemplate<String,Object> redisTemplate;

    @Value("${max.game.minutes}")
    private static int  maxGameMinutes;

    /* 把redis operation 缓存起来，因为 redisTemplate.boundxxxxOps 都是new 个对象*/
    /* BoundKeyOperations 每个key 对应一个对象，如果不缓存起来，每次操作redis都是新建一个BoundKeyOperations对象，即便是同一个key
    *  就算我在内存里把这些都缓存起来，也是每个key 都对应一个 BoundKeyOperations 对象。而opsForXXXX 除了hash 是新增对象，其他的都是
    * redisTemplate 的final 对象域。 所以，只缓存一个 opsForHash即可*/

//    @Deprecated
//    static Map<String,BoundKeyOperations<String>> hashKey2Operation = new HashMap<>();

    /**/
    private final static HashOperations opsForHash = redisTemplate.opsForHash();

    /**
     * 带超时时间 的 hash set操作，如果在用户（父母）输入登录时，用户（父母）输入
     * 时间单位固定是 分钟
     * @param hkey 大key
     * @param key 小key
     * @param value value 值
     */
    public static void hsetWithTTL(String hkey,String key,String value, int customMaxGameTime) {

        opsForHash.put(hkey,key,value);
        redisTemplate.expire(hkey, customMaxGameTime == 0 ? maxGameMinutes : customMaxGameTime , TimeUnit.MINUTES);
    }

    /**
     * 带默认超时时间 的 String set操作，默认超时时间在配置 文件里面配置
     * 时间单位固定是 分钟
     * @param key 小key
     * @param value value 值
     */
    public static void setWithTTL(String key,Object value, int customMaxGameMinutes) {
        redisTemplate.opsForValue().set(key,value);
        redisTemplate.expire(key, customMaxGameMinutes == 0 ? maxGameMinutes : customMaxGameMinutes , TimeUnit.MINUTES);
    }

    /**
     * String 类型 操作
     * @param token
     * @return
     */
    public static boolean isTokenValid(String token) {
        return redisTemplate.hasKey(token);
    }

}
