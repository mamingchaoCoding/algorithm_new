package individual.mamingchao.scratch_story.dao;

import individual.mamingchao.scratch_story.entity.Story;
import individual.mamingchao.scratch_story.entity.StoryExample;

import java.util.List;

public interface StoryMapper {

    List<Story> queryAllStory();

    int deleteByPrimaryKey(Short id);

    int insert(Story record);

    int insertSelective(Story record);

    List<Story> selectByExample(StoryExample example);

    Story selectByPrimaryKey(Short id);

    int updateByPrimaryKeySelective(Story record);

    int updateByPrimaryKey(Story record);

    @Deprecated
    int delByCode( String storyCode);
}