package individual.mamingchao.scratch_story.dao;

import individual.mamingchao.scratch_story.entity.StoryChapter;
import individual.mamingchao.scratch_story.entity.StoryChapterExample;

import java.util.List;

public interface StoryChapterMapper {
    int deleteByPrimaryKey(Short id);

    int insert(StoryChapter record);

    int insertSelective(StoryChapter record);

    List<StoryChapter> selectByExample(StoryChapterExample example);

    StoryChapter selectByPrimaryKey(Short id);

    int updateByPrimaryKeySelective(StoryChapter record);

    int updateByPrimaryKey(StoryChapter record);

    /**
     *
     * @param records
     * @return
     */
    int batchInsert(List<StoryChapter> records);

    List<StoryChapter> queryByCode(String storyCode);
}