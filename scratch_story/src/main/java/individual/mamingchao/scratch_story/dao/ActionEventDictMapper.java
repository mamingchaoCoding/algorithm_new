package individual.mamingchao.scratch_story.dao;

import individual.mamingchao.scratch_story.entity.ActionEventDict;
import individual.mamingchao.scratch_story.entity.ActionEventDictExample;

import java.util.List;

public interface ActionEventDictMapper {
    int deleteByPrimaryKey(Short id);

    int insert(ActionEventDict record);

    int insertSelective(ActionEventDict record);

    List<ActionEventDict> selectByExample(ActionEventDictExample example);

    ActionEventDict selectByPrimaryKey(Short id);

    int updateByPrimaryKeySelective(ActionEventDict record);

    int updateByPrimaryKey(ActionEventDict record);
}