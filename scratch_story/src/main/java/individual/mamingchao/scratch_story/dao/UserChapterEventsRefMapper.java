package individual.mamingchao.scratch_story.dao;

import individual.mamingchao.scratch_story.entity.UserChapterEventsRef;
import individual.mamingchao.scratch_story.entity.UserChapterEventsRefExample;

import java.util.List;

public interface UserChapterEventsRefMapper {
    int deleteByPrimaryKey(Short id);

    int insert(UserChapterEventsRef record);

    int insertSelective(UserChapterEventsRef record);

    List<UserChapterEventsRef> selectByExample(UserChapterEventsRefExample example);

    UserChapterEventsRef selectByPrimaryKey(Short id);

    int updateByPrimaryKeySelective(UserChapterEventsRef record);

    int updateByPrimaryKey(UserChapterEventsRef record);




    int haveBeenDone(short id);
}