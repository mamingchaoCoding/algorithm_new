package individual.mamingchao.scratch_story.dao;

import individual.mamingchao.scratch_story.entity.ActionEvents;
import individual.mamingchao.scratch_story.entity.ActionEventsExample;

import java.util.List;

public interface ActionEventsMapper {
    @Deprecated
    int deleteByPrimaryKey(Short id);
    @Deprecated
    int insert(ActionEvents record);

    @Deprecated
    int insertSelective(ActionEvents record);

    List<ActionEvents> selectByExample(ActionEventsExample example);

    ActionEvents selectByPrimaryKey(Short id);

    @Deprecated
    int updateByPrimaryKeySelective(ActionEvents record);

    @Deprecated
    int updateByPrimaryKey(ActionEvents record);



    int batchSave(List<ActionEvents> actionEvents);

    int delByUserChapterRefCode(String userChapterRefCode);
}