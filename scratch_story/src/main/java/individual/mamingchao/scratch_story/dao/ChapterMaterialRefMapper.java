package individual.mamingchao.scratch_story.dao;

import java.util.List;

public interface ChapterMaterialRefMapper {
    int deleteByPrimaryKey(Short id);

    int insert(ChapterMaterialRef record);

    int insertSelective(ChapterMaterialRef record);

    List<ChapterMaterialRef> selectByExample(ChapterMaterialRefExample example);

    ChapterMaterialRef selectByPrimaryKey(Short id);

    int updateByPrimaryKeySelective(ChapterMaterialRef record);

    int updateByPrimaryKey(ChapterMaterialRef record);
}