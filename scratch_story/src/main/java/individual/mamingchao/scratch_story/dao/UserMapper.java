package individual.mamingchao.scratch_story.dao;

import individual.mamingchao.scratch_story.dto.StoryChapterDto;
import individual.mamingchao.scratch_story.entity.ActionEventDict;
import individual.mamingchao.scratch_story.entity.User;
import individual.mamingchao.scratch_story.entity.UserExample;
import org.apache.ibatis.annotations.Param;

import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

public interface UserMapper {
    int deleteByPrimaryKey(Short id);

    int insert(User record);

    int insertSelective(User record);

    List<User> selectByExample(UserExample example);

    User selectByPrimaryKey(Short id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    /**
     *
     * @param account
     * @param password
     * @return
     */
    User getByAccountAndPassword(@Param("account") String account, @Param("password") String password);

    /**
     *
     * @param userCode
     * @return
     */
    TreeSet<StoryChapterDto> queryByUserCode(String userCode);

    LinkedList<ActionEventDict> queryByUserChapterCode(@Param("userCode") String userCode, @Param("chapterCode") String chapterCode);
}