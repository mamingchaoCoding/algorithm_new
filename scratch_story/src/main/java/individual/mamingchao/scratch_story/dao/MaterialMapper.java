package individual.mamingchao.scratch_story.dao;

import java.util.List;

public interface MaterialMapper {
    int deleteByPrimaryKey(Short id);

    int insert(Material record);

    int insertSelective(Material record);

    List<Material> selectByExample(MaterialExample example);

    Material selectByPrimaryKey(Short id);

    int updateByPrimaryKeySelective(Material record);

    int updateByPrimaryKey(Material record);

    /**
     *
     * @param chapterCode
     * @return
     */
    List<Material> queryByChapterCode(String chapterCode);

}