package individual.mamingchao.scratch_story.exception;

import individual.mamingchao.scratch_story.common.CodeMsg;
import lombok.Data;

/**
 * Created by mamingchao on 2020/12/28.
 */
@Data
public class GenericResponse {
    private int responseCode;
    private String responseMsg;
    private Object data;

    public GenericResponse(Object data){
        this.responseCode = CodeMsg.SUCCESS.getCode();
        this.responseMsg = CodeMsg.SUCCESS.getMsg();
        this.data = data;
    }

    public GenericResponse(CodeMsg codeMsg){
        this.responseCode = codeMsg.getCode();
        this.responseMsg = codeMsg.getMsg();
        this.data = null;
    }


}
