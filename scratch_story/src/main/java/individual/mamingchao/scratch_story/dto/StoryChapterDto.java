package individual.mamingchao.scratch_story.dto;

import lombok.Data;

@Data
@Deprecated
public class StoryChapterDto extends BaseDto{

    private String storyCode;

    private String bgImageUrl;

    private byte chapterNo;

    /* 本章节故事是否启用 */
    private boolean enable;

    /* 用户是否 完成了本章节*/
    private boolean userHaveDone;


}