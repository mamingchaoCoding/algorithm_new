package individual.mamingchao.scratch_story.dto;

import lombok.Data;

import java.util.Date;

/**
 *
 */
@Data
public class ActionEvent {

    private String eventDictName;

    private int eventOrder;

    private String actionType;

    private Date createTime;

    private String eventDictMemo;
}