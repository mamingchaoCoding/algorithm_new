package individual.mamingchao.scratch_story.dto;

import lombok.Data;
import lombok.NonNull;

@Data
public class UserInfo {

    @NonNull
    private String name;
    @NonNull
    private String cellPhone;
    @NonNull
    private String password;

    private String avatarUrl;
}