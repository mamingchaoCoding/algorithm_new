package individual.mamingchao.scratch_story.dto;

import lombok.Data;

@Data
public class ChapterMaterial {

    private String materialName;

    private String materialType;

    private String imageUrl;

    private String memo;

}