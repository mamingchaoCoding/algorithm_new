package individual.mamingchao.scratch_story.dto;

import lombok.Data;

import java.util.Date;

/**
 * Created by mamingchao on 2020/12/25.
 */
@Data
public class BaseDto  {

    private short id;

    private String code;

    private String name;

    private Date createTime;

    private String memo;
}
