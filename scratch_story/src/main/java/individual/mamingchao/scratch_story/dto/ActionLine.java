package individual.mamingchao.scratch_story.dto;

import lombok.Data;

@Data
public class ActionLine {

    private String actionEventsCode;

    private String eventDictCode;

    private int eventOrderNum;

}