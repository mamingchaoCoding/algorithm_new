package individual.mamingchao.scratch_story.dto;

import lombok.Data;
import lombok.NonNull;

import java.util.LinkedList;

@Data
public class UserChapterEvents {


    @NonNull
    private short id;
    @NonNull
    private  String code;
    @NonNull
    private String chapterCode;
    @NonNull
    private String userCode;
    /* 用户是否 完成了本章节*/
    @NonNull
    private boolean haveDone;



    private String bgImageUrl;

    private byte chapterNo;

    /* 本章节故事是否启用 */
    private boolean enable;

    LinkedList<ActionLine> actionLines;
}