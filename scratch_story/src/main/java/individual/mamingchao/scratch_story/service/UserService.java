package individual.mamingchao.scratch_story.service;

import individual.mamingchao.scratch_story.dto.UserChapterEvents;
import individual.mamingchao.scratch_story.dto.UserInfo;
import individual.mamingchao.scratch_story.exception.GenericResponse;

/**
 * Created by mamingchao on 2020/12/25.
 */
public interface UserService {

    /**
     * 注册用户
     * @param userInfo
     * @return
     */
    boolean register(UserInfo userInfo);

    /**
     * 用户登录
     * @param account 账户
     * @param md5Password md5后的用户密码
     * @param signature SHA-1(key1=value1&key2=value2&salt)
     * @return
     */
    GenericResponse login(String account, String md5Password, String signature, int maxGameMinutes);

    /**
     * 用户 登出
     * @param token 用户token
     * @return
     */
    boolean loginOut(String token);

    /**
     * 返回更新后的user信息
     * @return
     */
    GenericResponse updUsr(UserInfo user,String token);

    /**
     * 通过token 获取 用户信息
     * @param token
     * @return
     */
    GenericResponse getUserInfo(String token);


    /**
     * 通过用户编码 章节编码 获取 顺序的章节时间链表
     * 顺序就是 order by chapter_no
     * @param userCode
     * @return
     */
    GenericResponse queryByUserChapterCode(String userCode, String chapterCode);


    /**
     * 用户 保存章节的事件链表
     * 这里包括 新增保存、更新保存
     * @param userChapterEvents
     * @return
     */
    boolean saveUserChapterEventList(UserChapterEvents userChapterEvents);


}
