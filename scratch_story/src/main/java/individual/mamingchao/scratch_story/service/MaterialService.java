package individual.mamingchao.scratch_story.service;

import java.util.List;

/**
 * Created by mamingchao on 2020/12/25.
 */
public interface MaterialService {

    /**
     * 保存新增材料
     * @param material
     * @return
     */
    boolean saveMaterial(MaterialDto material);

    /**
     * 删除材料信息
     * @param id
     * @return 返回成功与否
     */
    boolean delMaterial(short id);

    /**
     * 更新 材料信息
     * @param material
     * @return 返回更新后的 材料信息
     */
    MaterialDto updMaterial(MaterialDto material);

    /**
     * 通过 章节编码获取该章节 所有可用的 材料
     * @param chapterCode
     * @return
     */
    List<MaterialDto> queryByChapterCode(String chapterCode);


}
