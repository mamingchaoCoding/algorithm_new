package individual.mamingchao.scratch_story.service.impl;

import individual.mamingchao.scratch_story.common.MaterialType;
import individual.mamingchao.scratch_story.service.FtpService;
import org.springframework.stereotype.Service;

/**
 * Created by mamingchao on 2020/12/25.
 */
@Service
public class FtpServiceImpl implements FtpService{

    /**
     * 上传 该程序服务本地所在的 文件到 ftp 服务器
     *
     * @param fileName      文件名称
     * @param localFilePath 文件在该程序服务所在的server 的本地地址
     * @param userCode      用户编码，组成ftp 存放文件路径地址
     * @param type          文件类型
     * @return 文件在ftp服务器上的 访问url
     */
    @Override
    public String uploadMaterial(String fileName, String localFilePath, String userCode, MaterialType type) {
        return null;
    }

    /**
     * 删除 ftp上指定url的文件
     *
     * @param fileUrl
     * @return
     */
    @Override
    public boolean delMaterial(String fileUrl) {
        return false;
    }
}
