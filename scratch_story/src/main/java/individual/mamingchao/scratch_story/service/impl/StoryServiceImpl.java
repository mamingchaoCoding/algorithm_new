package individual.mamingchao.scratch_story.service.impl;

import individual.mamingchao.scratch_story.dao.StoryChapterMapper;
import individual.mamingchao.scratch_story.dao.StoryMapper;
import individual.mamingchao.scratch_story.dao.UserMapper;
import individual.mamingchao.scratch_story.dto.StoryChapterDto;
import individual.mamingchao.scratch_story.dto.StoryDto;
import individual.mamingchao.scratch_story.service.StoryService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

/**
 * Created by mamingchao on 2020/12/25.
 */
@Service
public class StoryServiceImpl implements StoryService {

    @Autowired
    private StoryMapper storyMapper;

    @Autowired
    private StoryChapterMapper storyChapterMapper;

    @Autowired
    private UserMapper userMapper;

    /**
     * 查询所有 故事列表
     *
     * @return 所有故事列表
     */
    @Override
    public List<StoryDto> queryAllStoryList() {
        List<Story> storyList = storyMapper.queryAllStory();

        //TODO 整理范型 公共方法
        List<StoryDto> result = new ArrayList<>();
        storyList.stream().forEach(story -> {
            result.add(this.convertStory2StoryDto(story, true));

        });
        return result;
    }


    /**
     * 新增一个故事
     *
     * @param storyDto 故事dto
     * @return 是否保存成功
     */
    @Override
    public boolean saveStory(StoryDto storyDto) {

        int num = storyMapper.insert(this.convertStoryDto2Story(storyDto, true));

        return num == 1;
    }

    /**
     * 新增一个故事
     *
     * @param storyDto 故事dto
     * @return 更新后的 storyDto
     */
    @Override
    public StoryDto updStory(StoryDto storyDto) {
        Assert.isNull(storyDto.getId(),"更新故事章节，缺少主键字段");
        int num = this.storyMapper.updateByPrimaryKeySelective(this.convertStoryDto2Story(storyDto, false));

        return num == 1 ? storyDto : null;
    }

    /**
     * 删除一个故事
     *
     * @param id
     * @return
     */
    @Override
    public boolean delStory(short id) {
        int num = this.storyMapper.deleteByPrimaryKey(id);
        return num == 1;
    }

    /**
     * 批量保存故事章节
     *
     * @param storyDtoChapters
     * @return
     */
    @Override
    public boolean saveStoryChapters(List<StoryChapterDto> storyDtoChapters) {

        ArrayList storyChapters = new ArrayList();
        storyDtoChapters.stream().forEach(item -> {
            storyChapters.add(this.convertStoryChapterDto2StoryChapter(item, true));
        });
        int num = this.storyChapterMapper.batchInsert(storyChapters);

        return num == storyDtoChapters.size();
    }

    /**
     * 根据故事编码，查询所有故事章节
     *
     * @param storyCode
     * @return
     */
    @Override
    public List<StoryChapterDto> queryByStoryCode(String storyCode) {
        List<StoryChapterDto> result = new ArrayList<>();

        List<StoryChapter> storyChapters = this.storyChapterMapper.queryByCode(storyCode);
        storyChapters.stream().forEach(item -> {
            result.add(this.convertStoryChapter2StoryChapterDto(item, true));
        });

        return result;
    }

    /**
     * 删除一个故事的章节
     *
     * @param id
     * @return
     */
    @Override
    public boolean delStoryChapter(short id) {
        int num = this.storyChapterMapper.deleteByPrimaryKey(id);
        return false;
    }

    /**
     * 更新故事章节
     *
     * @param storyChapter
     * @return
     */
    @Override
    public StoryChapterDto upStoryChapter(StoryChapterDto storyChapter) {
        Assert.isNull(storyChapter.getId(),"更新故事章节，缺少主键字段");
        int num = this.storyChapterMapper.updateByPrimaryKeySelective(this.convertStoryChapterDto2StoryChapter(storyChapter,false));

        return num == 1 ? storyChapter : null;
    }


    /**
     * 通过用户编码获取 顺序 故事章节
     * 顺序就是 order by chapter_no
     *
     * @param userCode
     * @return
     */
    @Override
    public TreeSet<StoryChapterDto> queryByUserCode(String userCode) {

        return this.userMapper.queryByUserCode(userCode);
    }


    /**
     * 将 StoryDto 转化成 Story
     * @param dto
     * @return
     */
    private Story convertStoryDto2Story(StoryDto dto, boolean gcParam) {
        Story storyEntity = new Story();
        BeanUtils.copyProperties(dto,storyEntity);

        if (gcParam)
            dto = null;// help GC
        return storyEntity;
    }


    /**
     * 将 Story 转化成 StoryDto
     * @param story
     * @return
     */
    private StoryDto convertStory2StoryDto(Story story, boolean gcParam) {
        StoryDto storyDto= new StoryDto();
        BeanUtils.copyProperties(story,storyDto);
        if (gcParam)
            story = null; // help GC
        return storyDto;
    }


    /**
     * 将 StoryChapterDto 转化成 StoryChapter
     * @param dto
     * @return
     */
    private StoryChapter convertStoryChapterDto2StoryChapter(StoryChapterDto dto, boolean gcParam) {
        StoryChapter storyEntity = new StoryChapter();
        BeanUtils.copyProperties(dto,storyEntity);

        if (gcParam)
            dto = null;// help GC
        return storyEntity;
    }

    /**
     * 将 StoryChapter 转化成 StoryChapterDto
     * @param story
     * @return
     */
    private StoryChapterDto convertStoryChapter2StoryChapterDto(StoryChapter story, boolean gcParam) {
        StoryChapterDto storyDto= new StoryChapterDto();
        BeanUtils.copyProperties(story,storyDto);
        if (gcParam)
            story = null; // help GC
        return storyDto;
    }
}
