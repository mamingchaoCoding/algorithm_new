package individual.mamingchao.scratch_story.service;

import individual.mamingchao.scratch_story.dto.StoryChapterDto;
import individual.mamingchao.scratch_story.dto.StoryDto;
import individual.mamingchao.scratch_story.dto.UserChapterEvents;

import java.util.List;
import java.util.TreeSet;

/**
 * Created by mamingchao on 2020/12/25.
 */
public interface StoryService {
    /**
     * 查询所有 故事列表
     * @return 所有故事列表
     */
    List<StoryDto> queryAllStoryList();

    /**
     * 新增一个故事
     * @param story 故事dto
     * @return 是否保存成功
     */
    boolean saveStory(StoryDto story);

    /**
     * 新增一个故事
     * @param story 故事dto
     * @return 更新后的 storyDto
     */
    StoryDto updStory(StoryDto story);

    /**
     * 删除一个故事
     * @param id
     * @return
     */
    boolean delStory(short id);

    /**
     * 批量报错故事章节
     * @param storyChapters
     * @return
     */
    boolean saveStoryChapters(List<StoryChapterDto> storyChapters);

    /**
     * 根据故事编码，查询所有故事章节
     * @param storyCode
     * @return
     */
    List<StoryChapterDto> queryByStoryCode(String storyCode);

    /**
     * 删除一个故事的章节
     * @param id
     * @return
     */
    boolean delStoryChapter(short id);

    /**
     * 更新故事章节
     * @param storyChapter
     * @return
     */
    StoryChapterDto upStoryChapter(StoryChapterDto storyChapter);

    /**
     * 通过用户编码获取 顺序 故事章节
     * 顺序就是 order by chapter_no
     * @param userCode
     * @return
     */
    TreeSet<UserChapterEvents> queryByUserCode(String userCode);

}
