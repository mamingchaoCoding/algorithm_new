package individual.mamingchao.scratch_story.service;

import individual.mamingchao.scratch_story.common.MaterialType;

/**
 * Created by mamingchao on 2020/12/25.
 */
public interface FtpService {

    /**
     * 上传 该程序服务本地所在的 文件到 ftp 服务器
     *
     * @param fileName 文件名称
     * @param localFilePath 文件在该程序服务所在的server 的本地地址
     * @param userCode 用户编码，组成ftp 存放文件路径地址
     * @param type 文件类型
     * @return 文件在ftp服务器上的 访问url
     */
    String uploadMaterial(String fileName, String localFilePath, String userCode, MaterialType type);

    /**
     * 删除 ftp上指定url的文件
     * @param fileUrl
     * @return
     */
    boolean delMaterial(String fileUrl);
}
