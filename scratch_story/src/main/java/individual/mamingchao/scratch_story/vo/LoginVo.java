package individual.mamingchao.scratch_story.vo;

import lombok.Data;

/**
 * Created by mamingchao on 2020/12/25.
 */

@Data
public class LoginVo {

    private String code;

    private String name;

    private String cellPhone;

    private String account;

    private String avatarUrl;

    private String token;
}
