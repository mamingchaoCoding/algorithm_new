package individual.mamingchao.scratch_story.entity;

import java.util.Date;

public class UserChapterEventsRef {
    private short id;

    private String chapterCode;

    private String userCode;

    private boolean haveDone;

    private Date createTime;

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public String getChapterCode() {
        return chapterCode;
    }

    public void setChapterCode(String chapterCode) {
        this.chapterCode = chapterCode == null ? null : chapterCode.trim();
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode == null ? null : userCode.trim();
    }

    public Boolean getHaveDone() {
        return haveDone;
    }

    public void setHaveDone(Boolean haveDone) {
        this.haveDone = haveDone;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}