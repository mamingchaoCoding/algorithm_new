package individual.mamingchao.scratch_story.entity;

import lombok.Data;

import java.util.Date;

@Data
public class ActionEvents {
    private short id;

    private String code;

    private String chapterCode;

    private String eventDictCode;

    private int eventOrder;

    private Date createTime;

    private Date updateTime;

    private String memo;

}