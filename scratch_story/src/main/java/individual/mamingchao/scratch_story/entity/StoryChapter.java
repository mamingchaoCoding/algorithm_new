package individual.mamingchao.scratch_story.entity;

import java.util.Date;

public class StoryChapter {
    private Short id;

    private String code;

    private String name;

    private String storyCode;

    private String bgImageUrl;

    private Boolean enable;

    private Byte chapterNo;

    private Date createTime;

    private String memo;

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getStoryCode() {
        return storyCode;
    }

    public void setStoryCode(String storyCode) {
        this.storyCode = storyCode == null ? null : storyCode.trim();
    }

    public String getBgImageUrl() {
        return bgImageUrl;
    }

    public void setBgImageUrl(String bgImageUrl) {
        this.bgImageUrl = bgImageUrl == null ? null : bgImageUrl.trim();
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Byte getChapterNo() {
        return chapterNo;
    }

    public void setChapterNo(Byte chapterNo) {
        this.chapterNo = chapterNo;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }
}