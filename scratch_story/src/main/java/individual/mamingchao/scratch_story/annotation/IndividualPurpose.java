package individual.mamingchao.scratch_story.annotation;

import java.lang.annotation.*;

/**
 * Created by mamingchao on 2020/12/24.
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface IndividualPurpose {

}
