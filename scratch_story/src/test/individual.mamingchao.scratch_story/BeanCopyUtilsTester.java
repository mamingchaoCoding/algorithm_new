package individual.mamingchao.scratch_story;

import individual.mamingchao.scratch_story.dto.StoryDto;
import individual.mamingchao.scratch_story.util.BeanCopyUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mamingchao on 2020/12/25.
 */
public class BeanCopyUtilsTester {
    public static void main(String[] args) {
        List<Story> source = new ArrayList<Story>(){
            {
                add(new Story(Short.valueOf("1"),"storycode1","storyName1","imageUrl1",new Date(),Byte.parseByte("0"),"1"));
                add(new Story(Short.valueOf("2"),"storycode2","storyName2","imageUrl2",new Date(),Byte.parseByte("1"),"2"));
                add(new Story(Short.valueOf("3"),"storycode3","storyName3","imageUrl3",new Date(),Byte.parseByte("2"),"3"));
            }
        };

        List<Object> result = BeanCopyUtils.getInstance().BeanListCopy(source, StoryDto.class);

        result.stream().forEach(item -> {
            System.out.println(item.toString());
        });
        System.out.println(1243);
    }
}
