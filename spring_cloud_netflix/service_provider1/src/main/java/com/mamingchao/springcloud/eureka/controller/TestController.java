package com.mamingchao.springcloud.eureka.controller;

import com.mamingchao.springcloud.eureka.api.UserApi;
import com.mamingchao.springcloud.eureka.Dto.SearchCondition;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by mamingchao on 2020/8/21.
 */
@RestController
public class TestController implements UserApi {

    @Value("${server.port}")
    private String port;

    private AtomicInteger counter = new AtomicInteger();

    @Override
    public String greeting() {

        return "Greeting for service provider 1";
    }

    @Override
    public String hello(String name) {
        return "Say hello to " + name;
    }

    /**
     * ribbonReadTimeoutTest
     * @return
     */
    @Override
    public String ribbonReadTimeoutTest() {


        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        System.out.println("This is the " + counter.incrementAndGet() + " times invoke provicer 1, method ribbonReadTimeoutTest");
        System.out.println("Current timestamp is "+ System.currentTimeMillis());
        return "My server port is "+ port + "and this is ribbon read timeout test!";
    }

    @PostMapping("/postLocation")
    public URI postLocation(@RequestBody SearchCondition searchCondition, HttpServletResponse resp) throws URISyntaxException{
        String url = "https://www.baidu.com/s?wd="+searchCondition.getKeyword();

        System.out.println(searchCondition.getKeyword());

        URI uri = new URI("https://www.baidu.com/s?wd="+searchCondition.getKeyword());

        Cookie cookie = new Cookie("testCookieKey","testCookieValue");
        resp.addHeader("Location",url);
        resp.addHeader("charset","utf-8");

        return uri;

    }
}
