package com.mamingchao.springcloud.eureka.Dto;

/**
 * Created by mamingchao on 2020/8/27.
 */
public class SearchCondition {

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    private String keyword;
}
