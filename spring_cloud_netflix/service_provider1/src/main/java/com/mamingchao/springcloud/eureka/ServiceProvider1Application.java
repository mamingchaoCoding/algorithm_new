package com.mamingchao.springcloud.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceProvider1Application {

	public static void main(String[] args) {
		SpringApplication.run(ServiceProvider1Application.class, args);
	}

}
