package com.mamingchao.springcloud.eureka.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by mamingchao on 2020/8/28.
 */
//@RequestMapping("/serviceProvider")
public interface UserApi {

    @GetMapping("/serviceProvider/getHi")
    String greeting();

    /**
     * 如果传参数，这个地方不带 RequestParam
     * 会报错，错误信息
     * 【[ProviderApi2#hello(String)]: [{"timestamp":"2020-08-28T06:15:05.036+00:00","status":405,"error":"Method Not Allowed","message":"","path":"/serviceProvider/sayHello"}]】
     * @param name
     * @return
     */
    @GetMapping("/serviceProvider/sayHello")
    String hello(@RequestParam("name") String name);

    /**
     *
     *
     * @return
     */
    @GetMapping("/serviceProvider/ribbonReadTimeoutTest")
    String ribbonReadTimeoutTest();
}
