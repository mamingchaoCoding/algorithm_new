package com.mamingchao.springcloud.eureka;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by mamingchao on 2020/8/21.
 */
@RestController
public class TestController {


    @GetMapping("/greeting")
    public String greeting() {


        return "Greeting for service provider 2";
    }
}
