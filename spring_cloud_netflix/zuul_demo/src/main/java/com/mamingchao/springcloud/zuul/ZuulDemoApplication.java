package com.mamingchao.springcloud.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * 通过 zuul 请求service client 的时候，service client的服务名一定要写小写，因为eureka自动给转的
 * 这个是 个坑
 */
@SpringBootApplication
@EnableZuulProxy
public class ZuulDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZuulDemoApplication.class, args);
	}

}
