package com.mamingchao.designmodel.singleton_model;

/**
 * Created by mamingchao on 2020/9/24.
 */
public class Multition {

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    private String field;

    @Override
    public String toString() {
        return "Multition{" +
                "field='" + field + '\'' +
                '}';
    }
}
