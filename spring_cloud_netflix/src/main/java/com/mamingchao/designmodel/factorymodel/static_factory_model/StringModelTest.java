package com.mamingchao.designmodel.factorymodel.static_factory_model;

import com.mamingchao.designmodel.factorymodel.normal_factory_model_2.Sender;

/**
 * Created by mamingchao on 2020/9/24.
 */
public class StringModelTest {

    public static void main(String[] args) {

        Sender sender = StaticModelSenderFactory.getMailSenderInstance();
        sender.send("static factory test");
    }

}
