package com.mamingchao.designmodel.factorymodel.normal_factory_model_2;

/**
 * Created by mamingchao on 2020/9/24.
 */
public class MailSender implements Sender{
    @Override
    public boolean send(String content) {

        System.out.println("邮件发送-- " + content);
        return true;
    }
}
