package com.mamingchao.spring.cyclicdependency;

/**
 * Created by mamingchao on 2020/7/22.
 */
public class ClassB {
    private ClassA a;

    private int age;



    public ClassA getA() {
        return a;
    }

    public void setA(ClassA a) {
        this.a = a;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
