package com.mamingchao.spring.cyclicdependency;

/**
 * Created by mamingchao on 2020/7/22.
 */
public class ClassA {

    private ClassB b;

    private String name;




    public ClassB getB() {
        return b;
    }

    public void setB(ClassB b) {
        this.b = b;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
