package com.mamingchao.springcloud.session;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by mamingchao on 2020/9/18.
 */
@RestController
public class TestController {

    @GetMapping("/hi")
    public String getStr(){

        System.out.println("来了老弟！");

        return "I am a normal web site!";
    }
}
