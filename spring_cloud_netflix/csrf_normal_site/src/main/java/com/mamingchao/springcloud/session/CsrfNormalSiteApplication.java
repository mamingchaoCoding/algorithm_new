package com.mamingchao.springcloud.session;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CsrfNormalSiteApplication {

	public static void main(String[] args) {
		SpringApplication.run(CsrfNormalSiteApplication.class, args);
	}

}
