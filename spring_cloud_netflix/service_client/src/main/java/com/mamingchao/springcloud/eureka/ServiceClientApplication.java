package com.mamingchao.springcloud.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
//@Configuration
@EnableFeignClients
@EnableCircuitBreaker
@EnableHystrixDashboard
public class ServiceClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceClientApplication.class, args);
	}

//	@Bean
//	@LoadBalanced
//	RestTemplate getRestTemplate() {
//		return new RestTemplate();
//	}
//
//	@Bean
//	IRule myRule() {
////        return new RoundRobinRule();
//		return new RandomRule();
////        return new RetryRule();
////        return new ResponseTimeWeightedRule();
//	}
}
