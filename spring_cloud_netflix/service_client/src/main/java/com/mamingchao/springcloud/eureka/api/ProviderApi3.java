package com.mamingchao.springcloud.eureka.api;

import com.mamingchao.springcloud.eureka.service.HystrixFallbackService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by mamingchao on 2020/8/27.
 */
@FeignClient(name = "serviceProvider",fallback = HystrixFallbackService.class)
public interface ProviderApi3 extends UserApi{

    @GetMapping("/client/customApi")
    String customApi();
}
