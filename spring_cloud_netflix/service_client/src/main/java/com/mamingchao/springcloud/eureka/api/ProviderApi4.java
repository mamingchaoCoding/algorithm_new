package com.mamingchao.springcloud.eureka.api;

import com.mamingchao.springcloud.eureka.service.HystrixFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * Created by mamingchao on 2020/8/27.
 */
@FeignClient(name = "serviceProvider",fallbackFactory = HystrixFallbackFactory.class)
public interface ProviderApi4 extends UserApi{

}
