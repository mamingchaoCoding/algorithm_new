package com.mamingchao.springcloud.eureka.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * Created by mamingchao on 2020/8/25.
 */
@Configuration
public class BeanConfig {

    /**
     * spring 管理的 bean默认是单例的
     * @LoadBalanced 注解是restTemplate 集成feign，实现负载均衡
     * @return
     */
    @Bean
    @LoadBalanced
    RestTemplate restTemplate() {

//        RestTemplate restTemplate = new RestTemplate();

//        restTemplate.getInterceptors().add(new RestTemplateInterceptor());
        return new RestTemplate();
    }

    /**
     * 配置文件 可以配置负载均衡策略
     * service_provider.ribbon.NFLoadBalancerRuleClassName = com.netflix.loadbalancer.RoundRobinRule
     *
     * 配置文件配置更方便；当然也可以通过下面的 spring 注入 配置负载均衡策略；
     * 不过显然是没有 配置文件更方便的
     */
//    @Bean
//    IRule getRule() {
//        return new RoundRobinRule();
////        return new RandomRule();
////        return new RetryRule();
////        return new ResponseTimeWeightedRule();
//    }
}
