package com.mamingchao.springcloud.eureka.service;

import com.mamingchao.springcloud.eureka.api.ProviderApi3;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by mamingchao on 2020/9/10.
 */
@Component
public class HystrixFallbackService implements ProviderApi3 {

    @Override
    public String greeting() {
        return "greeting fallback method has been demoted";
    }

    /**
     * 如果传参数，这个地方不带 RequestParam
     * 会报错，错误信息
     * 【[ProviderApi2#hello(String)]: [{"timestamp":"2020-08-28T06:15:05.036+00:00","status":405,"error":"Method Not Allowed","message":"","path":"/serviceProvider/sayHello"}]】
     *
     * @param name
     * @return
     */
    @Override
    public String hello(@RequestParam("name") String name) {
        return "hello fallback method has been demoted";
    }

    /**
     * @return
     */
    @Override
    public String ribbonReadTimeoutTest() {
        return "ribbonReadTimeoutTest fallback method has been demoted";
    }

    @Override
    public String customApi() {
        return null;
    }
}
