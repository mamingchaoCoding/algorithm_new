package com.mamingchao.springcloud.eureka.api;

import org.springframework.cloud.openfeign.FeignClient;

/**
 * Created by mamingchao on 2020/8/27.
 */
@FeignClient(name = "serviceProvider")
public interface ProviderApi2 extends UserApi{

}
