package com.mamingchao.springcloud.eureka.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by mamingchao on 2020/8/27.
 */
@FeignClient(name = "serviceProvider")
public interface ProviderApi1 {

    /**
     *
     * @return
     */
    @GetMapping("/getHi")
    String getHiProxy();

}
