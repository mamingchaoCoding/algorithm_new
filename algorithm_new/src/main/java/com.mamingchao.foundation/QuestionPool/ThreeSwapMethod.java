package com.mamingchao.foundation.QuestionPool;

/**
 * 三种 数值交换 的方法
 *
 * 其中，第一种方法1 也适合其他类型（基础类型、引用类型）
 * 后两张 只适合 数值类型
 * Created by mamingchao on 2021/3/9.
 */
public class ThreeSwapMethod {

    private static void swap(int a , int b) {
        int temp = a;
        a = b;
        b = temp;
        System.out.println("a - " + a + "\t b-" + b);
    }

    private static void swap1 (int a, int b) {

        a = a + b;
        b = a - b;
        a = a - b;

        System.out.println("a - " + a + "\t b-" + b);
    }

    private static void swap2 (int a, int b) {
        a = a^b;
        b = a^b;
        a = a^b;
        System.out.println("a - " + a + "\t b-" + b);
    }

    public static void main(String[] args) {
        int a = 5 ;
        int b = 9 ;

        swap(a,b);
        swap1(a,b);
        swap2(a,b);

    }
}
