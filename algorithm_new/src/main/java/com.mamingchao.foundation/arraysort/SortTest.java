package com.mamingchao.foundation.arraysort;

import com.mamingchao.foundation.arraysort.buckesort.radixsort.RadixSortImpl;

/**
 *
 * Created by mamingchao on 2020/11/13.
 */
public class SortTest {

    public static void main(String[] args) {
        int[] arr = new int[]{5,3,6,1,8,2,1,0};
        int[] arr2 = new int[]{15,3,6,11,8,2,1,0};
        int[] arrForRadix = {432,4339,521,78,695,252,113};

//        SortProcesser.sort(arr,0,arr.length-1,new BubbleSortImpl());
//        SortProcesser.sort(arr,0,arr.length-1,new ClassicInsertSortImpl());
        SortProcesser.sort(arrForRadix,0,arrForRadix.length-1,new RadixSortImpl());

        SortProcesser.printArray(arrForRadix);
    }
}
