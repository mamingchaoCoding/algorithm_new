package com.mamingchao.foundation.arraysort;


/**
 * Created by mamingchao on 2021/3/19.
 */
public class SortProcesser {

    public static void sort(int[] arr, int start, int end, Sortable sortable){
        sortable.sort(arr,start,end);
    }

    public static void printArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }


}
