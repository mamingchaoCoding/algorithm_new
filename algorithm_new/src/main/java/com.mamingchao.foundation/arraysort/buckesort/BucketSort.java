package com.mamingchao.foundation.arraysort.buckesort;

/**
 * Created by mlamp on 2024/5/31.
 * 设置一个定量的数组当作空桶子。
 * 寻访序列，并且把项目一个一个放到对应的桶子去。
 * 对每个不是空的桶子进行排序。
 * 从不是空的桶子里把项目再放回原来的序列中。
 *
 * 重点： 把每个元素，哈希映射到每个桶里；这个hash函数很重要，要尽量保证每个桶里装的元素数量比较均匀
 *      1、在额外空间充足的情况下，尽量增大桶的数量
 *      2、使用的映射函数能够将输入的 N 个数据均匀的分配到 K 个桶中
 */
public class BucketSort {
}
