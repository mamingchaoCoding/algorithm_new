package com.mamingchao.foundation.arraysort.buckesort.radixsort;

/**
 * Created by mlamp on 2024/6/28.
 */
public class GenereteDigitNumUtil {


    /**
     *
     * @param num 原始长度 大于1的数字
     * @param digitPos 欲获取digit的位置，从低位向高位计算 ,取值范围 0 ->个位，1-->十位 2-->百位
     * @return 获取一个10进制指定digitPos位置的 数值
     */
    public static int getDigitNumOtherWay(int num, int digitPos) {
        final int DECIMAL = 10;


        int division = new Double(Math.pow(DECIMAL ,digitPos)).intValue();
        return (num/division)%10;
    }

    /**
     * 获取 > 1的数字，获取指定位置的值（个位，十位，百位） 优化版
     * 这个实现的版本，为 截取 从个位 到所需高位的新数字（originalNum%(10*model)）
     * 下一步，再只留高位的数字（/models）
     * 等同于下面的getSingleNum 方法
     * @param originalNum 十进制的数值
     * @param model 个位 -> 1; 十位 -》10；百位 -》 100
     * @return 获取一个10进制指定digitPos位置的 数值
     */
    public static int getDigitNum(int originalNum, int model) {
        return originalNum%(10*model)/model;
    }

    /**
     * 长度 > 1的数字，获取指定位置的值（个位，十位，百位）
     *
     * 十进制的方法，等同于上面的 getDigitNum
     * @param originalNumItem 十进制的数值
     * @param model 1表示取originalNumItem的个位值，10表示取originalNumItem的十位值，100表示取originalNumItem的百位值，以此类推
     * @return 获取一个10进制指定digitPos位置的 数值
     */
    public static int getSingleNum(int originalNumItem,int model){
        if (1 == model) {
            return originalNumItem%10;
        }

        return (originalNumItem%(10*model) - originalNumItem%model)/model;
    }

    public static void main(String[] args) {

        System.out.println(getDigitNum(278, 1000));

//        for (int i = 100; i < 199; i++) {
//            System.out.println(getDigitNum(i, 10));
//            System.out.println("=================================");
//            System.out.println(getDigitNumOtherWay(i, 10));
//        }
    }
}
