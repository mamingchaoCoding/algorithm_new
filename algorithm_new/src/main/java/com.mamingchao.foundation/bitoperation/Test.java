package com.mamingchao.foundation.bitoperation;

/**
 * | 或运算
 * & 与运算
 * ^ 异或运算
 * << 带符号向左偏移，相当于*2 带符号的意思，高位原来是什么，继续补什么
 * >> 带符号向右偏移，相当于/2
 * <<< 不带符号向左偏移。不带符号的意思，是偏移完，高位补0
 * >>> 不带符号向右偏移
 * Created by mamingchao on 2020/11/13.
 */
public class Test {
    public static void main(String[] args) {
        int i = 19;
        int j = 19;

        System.out.println(i*2+1 == (j<<1|1));
    }
}
